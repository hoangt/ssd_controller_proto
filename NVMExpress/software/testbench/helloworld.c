#include <stdio.h>
#include <stdint.h>
#include "platform.h"
#include "xparameters.h"
#include "xtime_l.h"
#include "xil_printf.h"
#include "xil_cache.h"

#define NO_SQUEUES 17
#define NO_CQUEUES 17
#define SQ_ENTRY_SIZE 64
#define CQ_ENTRY_SIZE 16
#define QSIZE 5
#define DSTRD 0
#define NO_PRPs 16
#define MPS 0

#define AQA (((QSIZE - 1) << 16) | (QSIZE - 1))

/* completion dword 2 */
#define GET_SQ_HEAD_POINTER_COMPL(x) (x.dword2 & 0xFFFF)
#define GET_SQID_COMPL(x) ((x.dword2 >> 16) & 0xFFFF)
/* completion dword 3 */
#define GET_PHASE_TAG_COMPL(x) ((x.dword3 >> 16) & 1)

#define SET_OPCODE_CMD(cmd, opcode) cmd.dword0 = (cmd.dword0 & ~(0xFF)) | opcode
#define SET_COMMAND_ID_CMD(cmd, id) cmd.dword0 = (cmd.dword0 & ~(0xFFFF << 16)) | (id << 16)

#define GET_PHASE_TAG_SOFT(x) (x & 1)

#define CONV_ADDR(addr) ((uint32_t *) (((uint32_t) addr) + 0x01000000))

typedef struct commands {
	uint32_t dword0;
	uint32_t dword1;
	uint32_t dword2;
	uint32_t dword3;
	uint32_t dword4;
	uint32_t dword5;
	uint64_t prp1;
	uint64_t prp2;
	uint32_t dword10;
	uint32_t dword11;
	uint32_t dword12;
	uint32_t dword13;
	uint32_t dword14;
	uint32_t dword15;
} command_t;

typedef struct completions {
	uint32_t dword0;
	uint32_t dword1;
	uint32_t dword2;
	uint32_t dword3;
} completion_t;

//void print(char *str);

volatile uint32_t *config_base = (uint32_t *) XPAR_CONFIG_INTERFACE_0_BASEADDR;

uint32_t *sq_head[NO_SQUEUES];
uint32_t *sq_tail[NO_SQUEUES];
uint32_t *cq_head[NO_SQUEUES];
uint16_t sq_cmd_id[NO_SQUEUES] = {0};
uint8_t phase_tags[NO_SQUEUES];

uint32_t prp_used[NO_PRPs] = {0};
uint32_t controller_configuration = (6 << 20) | (6 << 16) | (0 << 14) |
	(0 << 11) | (MPS << 7) | (0 << 4) | 0;

XTime start, end;
uint64_t count;

uint64_t config_read(uint32_t address)
{
	uint64_t data;
	config_base[4] = address;
	uint32_t data_msb = config_base[1];
	uint32_t data_lsb = config_base[0];
	xil_printf("Configuration read of 0x%x%x from 0x%x\n", data_msb, data_lsb, address);
	data = data_msb;
	data = (data << 32) | data_lsb;
	return data;
}

void config_write(uint32_t address, uint64_t data)
{
	uint32_t data_msb = data >> 32;
	uint32_t data_lsb = data;
	config_base[4] = address;
	config_base[2] = data_lsb;
	config_base[3] = data_msb;
	config_base[5] = ~config_base[5];
	/* This print is necessary!  For timing? */
	xil_printf("Configuration write of 0x%x%x to 0x%x\n", data_msb, data_lsb, address);
}

uint32_t *get_sq_addr(uint16_t sqid)
{
	return (uint32_t *) ((sqid * 2) << 12);
}

uint32_t *get_cq_addr(uint16_t cqid)
{
	return (uint32_t *) ((cqid * 2 + 1) << 12);
}

uint8_t sq_empty(uint16_t sqid)
{
	return (sq_head[sqid] == sq_tail[sqid]);
}

uint8_t sq_full(uint16_t sqid)
{
	uint8_t normal_full = (uint32_t *) ((uint32_t) sq_tail[sqid] + SQ_ENTRY_SIZE) == sq_head[sqid];
	uint8_t wrap_full = sq_tail[sqid] == (uint32_t *) ((uint32_t) get_sq_addr(sqid) +
		(QSIZE - 1) * SQ_ENTRY_SIZE) && (sq_head[sqid] == get_sq_addr(sqid));
	return normal_full || wrap_full;
}

uint32_t get_cqid(uint16_t sqid)
{
	return sqid;
}

uint32_t allocate_prp()
{
	int i;
	for (i = 0; i < NO_PRPs; i++) {
		if (prp_used[i] == 0) {
			prp_used[i] = 1;
			return 0x10000 + (i << 12);
		} else if (i == NO_PRPs) {
			print("ERROR: No PRP free for allocation.  This is undefined.\n");
		}
	}
	return 0;
}

void free_prp(prp_id)
{
	prp_used[prp_id] = 0;
}

void sq_enq(command_t command, uint16_t sqid)
{
	xil_printf("\tsq_enq: Started.  (dword0: 0x%x)\n", command.dword0);
	if (sq_full(sqid))
		print("ERROR: Queue full! This is undefined!\n");

	*((command_t *) CONV_ADDR(sq_tail[sqid])) = command;

	print("\tsq_enq: Update Doorbells.\n");

	if (sq_cmd_id[sqid] == QSIZE - 1)
		sq_cmd_id[sqid] = 0;
	else
		sq_cmd_id[sqid]++;

	print("\tsq_enq: Update the Submission Queue Tail Doorbell.\n");
	config_write((0x1000 + (2 * sqid) * (4 << DSTRD)), sq_cmd_id[sqid]);

	print("\tsq_enq: Update sq_tail.\n");
	if (sq_tail[sqid] == (uint32_t *) ((uint32_t) get_sq_addr(sqid) + (QSIZE - 1) * SQ_ENTRY_SIZE))
		sq_tail[sqid] = get_sq_addr(sqid);
	else
		sq_tail[sqid] = (uint32_t *) ((uint32_t) sq_tail[sqid] + SQ_ENTRY_SIZE);

	print("Get start time.\n");
	config_write(0x1FFF, 0x33);
	XTime_GetTime(&start);
}

completion_t cq_deq(uint16_t cqid)
{
	completion_t completion = *((completion_t *) CONV_ADDR(cq_head[cqid]));
	xil_printf("\tcq_deq: Wait for valid completion at 0x%x. (cqid: %d, expected phase_tag: %d)\n",
		CONV_ADDR(cq_head[cqid]), cqid, GET_PHASE_TAG_SOFT(phase_tags[cqid]));
	while (GET_PHASE_TAG_COMPL(completion) != GET_PHASE_TAG_SOFT(phase_tags[cqid]));
	print("Get end time.\n");
	XTime_GetTime(&end);
	count = config_read(0x1FFF);
	print("\tcq_deq: Valid completion received.\n");
	if (cq_head[cqid] == (uint32_t *) ((uint32_t) get_cq_addr(cqid) + (QSIZE - 1) * CQ_ENTRY_SIZE)) {
		/* wrap around condition */
		cq_head[cqid] = get_cq_addr(cqid);
		phase_tags[cqid] = ~phase_tags[cqid];
	} else {
		cq_head[cqid] = (uint32_t *) ((uint32_t) cq_head[cqid] + CQ_ENTRY_SIZE);
	}

	print("\tcq_deq: Update Submission Queue head pointer.\n");
	sq_head[GET_SQID_COMPL(completion)] = (uint32_t *) ((uint32_t) get_sq_addr(GET_SQID_COMPL(completion)) +
		GET_SQ_HEAD_POINTER_COMPL(completion) * SQ_ENTRY_SIZE);

	print("\tcq_enq: Update the Completion Queue Head Doorbell register.\n");
	config_write((0x1000 + (2 * cqid + 1) * (4 << DSTRD)),
		((uint32_t) cq_head[cqid] - (uint32_t) get_cq_addr(cqid)) /
		CQ_ENTRY_SIZE);

	return completion;
}

command_t make_cmd( uint8_t opcode, uint64_t prp1, uint64_t prp2,
	uint32_t cdw10, uint32_t cdw11, uint32_t cdw12, uint16_t sqid)
{
	command_t command = {0};
	command.dword10 = cdw10;
	command.dword11 = cdw11;
	command.dword12 = cdw12;
	command.prp1 = prp1;
	command.prp2 = prp2;
	SET_OPCODE_CMD(command, opcode);
	SET_COMMAND_ID_CMD(command, sq_cmd_id[sqid]);
	//xil_printf("command dword0: 0x%x\n", command.dword0);
	return command;
}

void initialize()
{
	completion_t completion;
	int i, j;

	print("Clean queue space.\n");
	volatile unsigned int* base_address = (unsigned int*) 0x01000000;
	for (i = 0; i < 6; i++) {
		volatile unsigned int* queue_base_address = (unsigned int*) ((unsigned int) base_address + ((2 * i + 1) << 12));
		for (j = 0; j < 20; j++) {
			volatile unsigned int* dword_address = (unsigned int*) ((unsigned int) queue_base_address) + j;
			*dword_address = 0;
		}
	}


    print("Initialize submission queue head, tail pointers and phase tags.\n");
    for (i = 0; i < NO_SQUEUES; i++) {
    	sq_head[i] = get_sq_addr(i);
    	sq_tail[i] = get_sq_addr(i);
    	phase_tags[i] = 0xFF;
    }
    print("Initialize completion queue head pointer.\n");
    for (i = 0; i < NO_CQUEUES; i++)
    	cq_head[i] = get_cq_addr(i);

	print("Testing the controller ready status until it is zero and thus the controller can be enabled.\n");
	while ((config_read(0x1c) & 1) != 0) print(".\n");

	print("Read the capabilities of the NVM Controller.  TODO\n");
	// TODO

	print("Set the Admin Queue Attributes of the NVM Controller.\n");
	config_write(0x24, AQA);

	print("Set the Admin Submission Queue Base Address of the NVM Controller.\n");
	config_write(0x28, (uint32_t) get_sq_addr(0));

	print("Set the Admin Completion Queue Base Address of the NVM Controller.\n");
	config_write(0x30, (uint32_t) get_cq_addr(0));

	print("Set the Controller Configuration of the NVM Controller.\n");
	config_write(0x14, controller_configuration);

	print("Send command to enable the NVM Controller.\n");
	controller_configuration = controller_configuration | 1;
	config_write(0x14, controller_configuration);

	print("Wait for NVM Controller to be enabled.\n");
	while ((config_read(0x1c) & 1) != 1) print(".\n");

	print("Identify Controller.\n");
	sq_enq(make_cmd(0x6, allocate_prp(), 0, 0x1, 0, 0, 0), 0);
	completion = cq_deq(get_cqid(0));
	print("TODO: Check result.  Free PRP.\n");

	print("Get Active Namespaces.\n");
	sq_enq(make_cmd(0x6, allocate_prp(), 0, 0x2, 0, 0, 0), 0);
	completion = cq_deq(get_cqid(0));
	print("TODO: Check result.  Free PRP.\n");

	print("Identify Namespaces.  TODO\n");

	print("Request the number of queues used by the host.\n");
	sq_enq(make_cmd(0x9, 0, 0, 0x7, 0x0, 0, 0), 0);
	completion = cq_deq(get_cqid(0));
	print("TODO: Check result.\n");

	print("Create the required number of completion queues (one for now).\n");
	sq_enq(make_cmd(0x5, (uint32_t) get_cq_addr(1), 0, ((QSIZE - 1) << 16) | 0x1,
		(1 << 16) | (1 << 1) | 1, 0, 0), 0);
	completion = cq_deq(get_cqid(0));
	print("TODO: Check result.\n");

	print("Create the required number of submission queues (one for now).\n");
	sq_enq(make_cmd(0x1, (uint32_t) get_sq_addr(1), 0, ((QSIZE - 1) << 16) | 0x1,
		(get_cqid(1) << 16) | 1, 0, 0), 0);
	completion = cq_deq(get_cqid(0));
	print("TODO: Check result.\n");

	print("Initialization finished.\n");
}

void test_transmission(uint32_t nr_pages)
{
	completion_t completion;
	int i, j;
	uint64_t mm2nand_count = 0, nand2mm_count = 0;
	int received_correct_data = 1;

	//print("Setting up some test data for a data transmission.\n");
	for (i = 0; i < nr_pages * 1024; i++) {
		((uint32_t *) 0x01200000)[i] = i;
		((uint32_t *) 0x01300000)[i] = 0xABBAABBA;
		((uint32_t *) 0x01400000)[i] = 0xBABABABA;
	}

	/*
	for (i = 0; i < nr_pages * 1024; i++)
		xil_printf("Initialized: (%d) 0x%x -> 0x%x -> 0x%x.\n", i, ((uint32_t *) 0x01200000)[i], ((uint32_t *) 0x01300000)[i], ((uint32_t *) 0x01400000)[i]);
	*/

    for (i = 1; i < nr_pages; i++) {
		uint32_t *prp_list1_addr = (uint32_t *) 0x01500000;
		uint32_t *prp_list2_addr = (uint32_t *) 0x01501000;
		prp_list1_addr[(i - 1) * 2] = 0x00200000 + i * 4096;
		prp_list1_addr[(i - 1) * 2 + 1] = 0;
		prp_list2_addr[(i - 1) * 2] = 0x00400000 + i * 4096;
		prp_list2_addr[(i - 1) * 2 + 1] = 0;
	}

	/*
	base_address = (unsigned int*) 0x0102F000;
	for (i = 0; i < 15; i++) {
		volatile unsigned int *dword_address = base_address + (i * 2 + 1);
		*dword_address = 0x01021000 + i * 4096;
	}

	base_address = (unsigned int*) 0x0103F000;
	for (i = 0; i < 15; i++) {
		volatile unsigned int *dword_address = base_address + (i * 2 + 1);
		*dword_address = 0x01041000 + i * 4096;
	}
	*/

    xil_printf("Start Hardware Data Transmission Test of %d pages.\n", nr_pages);

	print("Transfer from Main Memory to NAND.\n");
	if (nr_pages < 3)
		sq_enq(make_cmd(0x1, 0x00200000, 0x00201000, 0x00300000, 0, nr_pages - 1, 1), 1);
	else
		sq_enq(make_cmd(0x1, 0x00200000, 0x00500000, 0x00300000, 0, nr_pages - 1, 1), 1);
	completion = cq_deq(get_cqid(1));

	if ((completion.dword3 >> 27) == 0)
		xil_printf("Success received for command id %d.\n", completion.dword3 & 0xFFFF);
	else
		xil_printf("ERROR received for command id %d.\n", completion.dword3 & 0xFFFF);
	mm2nand_count = count;
	printf("Main Memory to NAND transmission done. Speed %lld Mbit/s (%lld Mbit/s)\n",
			(nr_pages * 4096 * 8) / ((end - start) / (COUNTS_PER_SECOND / 1000000)), (nr_pages * 4096 * 8 * 1000) / count);


	print("Transfer from NAND to Main Memory.\n");
	if (nr_pages < 3)
		sq_enq(make_cmd(0x2, 0x00400000, 0x00401000, 0x00300000, 0, nr_pages - 1, 1), 1);
	else
		sq_enq(make_cmd(0x2, 0x00400000, 0x00501000, 0x00300000, 0, nr_pages - 1, 1), 1);
	completion = cq_deq(get_cqid(1));

	if ((completion.dword3 >> 27) == 0)
		xil_printf("Success received for command id %d (dword2: 0x%x).\n", completion.dword3 & 0xFFFF, completion.dword2);
	else
		xil_printf("ERROR received for command id %d.\n", completion.dword3 & 0xFFFF);
	nand2mm_count = count;
	printf("NAND to Main Memory transmission done. Speed %lld Mbit/s (%lld Mbit/s)\n",
			(nr_pages * 4096 * 8) / ((end - start) / (COUNTS_PER_SECOND / 1000000)), (nr_pages * 4096 * 8 * 1000) / count);

	print("Check transferred data.\n");
	for (i = 0; i < nr_pages * 1024; i++) {
		if (((uint32_t *) 0x01200000)[i] != ((uint32_t *) 0x01400000)[i]) {
			xil_printf("Received wrong data (%d) 0x%x -> 0x%x -> 0x%x.\n", i, ((uint32_t *) 0x01200000)[i], ((uint32_t *) 0x01300000)[i], ((uint32_t *) 0x01400000)[i]);
			received_correct_data = 0;
		}
	}
	if (received_correct_data)
		print("Data test finished.  All data correct.\n");
	else
		print("Data test finished.  Wrong data received.\n");

	printf("py %d %lld %lld\n", nr_pages, mm2nand_count, nand2mm_count);
/*
	print("Start Software Data Transmission Test.\n");
	XTime_GetTime(&start);
	for (i = 0; i < 15 * 4096; i++)
		((uint32_t *) 0x01030000)[i] = ((uint32_t *) 0x01020000)[i];
	for (i = 0; i < 15 * 4096; i++)
		((uint32_t *) 0x01040000)[i] = ((uint32_t *) 0x01030000)[i];
	XTime_GetTime(&end);
	printf("Data Transmission Test done. Speed %lld Mbit/s\n",
			(2 * 15 * 4096 * 8) / ((end - start) / (COUNTS_PER_SECOND / 1000000)));
*/
}

int main()
{
	int i, j;
	Xil_DCacheDisable();
	XTime cur, now;
    init_platform();

    print("Hello World\n\r");
    initialize();
    for (i = 2; i < 3; i++) {
    	xil_printf("%d. Test.\n", i);
    	test_transmission(i);
    	XTime_GetTime(&cur);
    	do XTime_GetTime(&now); while (now - cur < 500000000);
    }
    //test_transmission(214);
    /*
    for (i = 1016; i < 1032; i++)
		xil_printf("%d:\t0x%x\t0x%x\t0x%x\n", i, ((uint32_t *) 0x01020000)[i], ((uint32_t *) 0x01030000)[i], ((uint32_t *) 0x01040000)[i]);
*/
    //for (i = 0; i < 32; i++)
    //		xil_printf("%d:\t0x%x\t0x%x\n", i, ((uint32_t *) 0x0102F000)[i], ((uint32_t *) 0x0103F000)[i]);

/*
	volatile unsigned int* base_address = (unsigned int*) 0x01000000;
	for (i = 0; i < 6; i++) {
		volatile unsigned int* queue_base_address = (unsigned int*) ((unsigned int) base_address + ((2 * i) << 12));
		for (j = 0; j < 20; j++) {
			volatile unsigned int* dword_address = (unsigned int*) ((unsigned int) queue_base_address) + j;
			xil_printf("0x%x -- 0x%x\n", dword_address, *dword_address);
		}
	}
*/
    print("EOE\n");
    return 0;
}
