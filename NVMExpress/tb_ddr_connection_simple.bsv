/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
module name: Testbench for DDR Connection
author name: Maximilian Singh
email id: maximilian.singh@student.kit.edu

This testbench tests the DDR connection.
*/

package tb_ddr_connection;

import DReg::*;

import global_definitions::*;

typedef enum {
	IDLE,
	SEND_WRITE_CMD,
	SEND_WRITE_CMD2,
	SEND_WRITE_DATA,
	SEND_WRITE_DATA2
} Tb_state_type deriving (Bits, Eq);

interface Ifc_tb_for_ddr_connection;
	interface Ifc_read_data_ddr ifc_read_data_ddr;
	interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
	interface Ifc_write_data_ddr ifc_write_data_ddr;
	interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
	interface Ifc_write_sts_ddr ifc_write_sts_ddr;
	method Action _reg_in(Bit#(32) _data);
	method Bit#(32) reg_out_();
	method Bit#(8) leds_();
endinterface

(* synthesize *)
(* always_ready *)
(* always_enabled *)
module mkTb_for_ddr_connection(Ifc_tb_for_ddr_connection);

Reg#(Tb_state_type) rg_tb_state <- mkReg(IDLE);

/* Registers used to control the hardware from processor. */
Reg#(Bit#(32)) rg_out_reg <- mkReg(0);
Reg#(Bit#(32)) rg_last_data <- mkReg(0);
Reg#(Bit#(32)) rg_address <- mkReg(32'h01000000);
Reg#(Bool) drg_restart <- mkDReg(False);

/* LED register which holds the hardware status. */
Reg#(Bit#(8)) rg_out_leds <- mkReg('hAA);

/* Wires of the Read Data interface of the Xilinx AXI Datamover IP */
Wire#(bit) wr_in_read_data_ddr_valid <- mkWire();
Wire#(Bit#(32)) wr_in_read_data_ddr_data <- mkWire();
Wire#(bit) wr_in_read_data_ddr_last <- mkWire();
Wire#(bit) dwr_out_read_data_ready <- mkDWire(1'b0);

/* Wires of the Read Command interface of the Xilinx AXI Datamover IP */
Reg#(bit) rg_out_read_cmd_ddr_valid <- mkReg(0);
Wire#(bit) wr_in_read_cmd_ddr_ready <- mkWire();
Reg#(UInt#(32)) rg_out_ddr_read_addr <- mkReg(0);
Reg#(UInt#(23)) rg_out_ddr_read_length <- mkReg(0);

/* Wires of the Write Data interface of the Xilinx AXI Datamover IP */
Reg#(bit) rg_out_write_data_ddr_valid <- mkReg(0);
Reg#(Bit#(32)) rg_out_write_data_ddr_data <- mkReg(0);
Reg#(bit) rg_out_write_data_ddr_last <- mkReg(0);
Wire#(bit) wr_in_write_data_ddr_ready <- mkWire();

/* Wires of the Write Command interfacee of the Xilinx AXI Datamover IP */
Reg#(bit) rg_out_write_cmd_ddr_valid <- mkReg(0);
Wire#(bit) wr_in_write_cmd_ddr_ready <- mkWire();
Reg#(UInt#(32)) rg_out_ddr_write_addr <- mkReg(0);
Reg#(UInt#(23)) rg_out_ddr_write_length <- mkReg(0);

/* Register for S2MM Status Interface */
Reg#(bit) rg_out_write_sts_ddr_ready <- mkReg(1'b0);
Wire#(bit) wr_in_write_sts_ddr_valid <- mkWire();
Wire#(Bit#(8)) wr_in_write_sts_ddr_data <- mkWire();


rule rl_tb_idle_state (rg_tb_state == IDLE);
	if (drg_restart == True)
		rg_tb_state <= SEND_WRITE_CMD;
endrule: rl_tb_idle_state

rule rl_tb_send_write_cmd_state (rg_tb_state == SEND_WRITE_CMD);
	rg_out_ddr_write_addr <= 0;
	rg_out_ddr_write_length <= 4;
	rg_out_write_cmd_ddr_valid <= 1;
	rg_tb_state <= SEND_WRITE_CMD2;
endrule: rl_tb_send_write_cmd_state

rule rl_tb_send_write_cmd2_state (rg_tb_state == SEND_WRITE_CMD2 &&
		wr_in_write_cmd_ddr_ready == 1'b1);
	rg_out_write_cmd_ddr_valid <= 0;
	rg_tb_state <= SEND_WRITE_DATA;
endrule: rl_tb_send_write_cmd2_state

rule rl_tb_send_write_data_state (rg_tb_state == SEND_WRITE_DATA);
	rg_out_write_data_ddr_data <= 'hDEADBEEF;
	rg_out_write_data_ddr_last <= 1'b1;
	rg_out_write_data_ddr_valid <= 1'b1;
	rg_tb_state <= SEND_WRITE_DATA2;
endrule: rl_tb_send_write_data_state

rule rl_tb_send_write_data_state2 (rg_tb_state == SEND_WRITE_DATA2 &&
		wr_in_write_data_ddr_ready == 1'b1);
	rg_out_write_data_ddr_valid <= 1'b0;
	rg_tb_state <= IDLE;
endrule: rl_tb_send_write_data_state2




/* --- Interfaces for the connection to the Xilinx AXI Datamover IP. --- */
/* Interface to read payload data from Xilinx AXI Datamover IP. */
interface Ifc_read_data_ddr ifc_read_data_ddr;
	method bit ready_();
		return dwr_out_read_data_ready;
	endmethod: ready_

	method Action _valid(bit _is_valid);
		wr_in_read_data_ddr_valid <= _is_valid;
	endmethod: _valid

	method Action _data_in(Bit#(32) _data);
		wr_in_read_data_ddr_data <= _data;
	endmethod: _data_in

	method Action _last(bit _is_last);
		wr_in_read_data_ddr_last <= _is_last;
	endmethod: _last
endinterface: ifc_read_data_ddr


/* Interface to send read request to the Xilinx AXI Datamover IP. */
interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
	method bit valid_();
		return rg_out_read_cmd_ddr_valid;
	endmethod: valid_

	method Bit#(72) data_();
		return {
			4'b0,  // reserved
			4'b0,  // command tag; not used
			  // start address, 'h01000000 is base address of the DDR
			pack(rg_out_ddr_read_addr + 'h01000000),
			1'b0,  // DRE ReAlignment Request; not used
			1'b1,  // end of frame
			6'b0	,  // DRE Stream Alignment; not used
			1'b1,  // INCR access type
			pack(rg_out_ddr_read_length)  // bytes to transfer
		};
	endmethod: data_

	method Action _ready(bit _is_ready);
		wr_in_read_cmd_ddr_ready <= _is_ready;
	endmethod: _ready
endinterface: ifc_read_cmd_ddr


/* Interface to write payload data to the Xilinx AXI Datamover IP. */
interface Ifc_write_data_ddr ifc_write_data_ddr;
	method bit valid_();
		return rg_out_write_data_ddr_valid;
	endmethod: valid_

	method Bit#(32) data_out_();
		return rg_out_write_data_ddr_data;
	endmethod: data_out_

	method bit last_();
		return rg_out_write_data_ddr_last;
	endmethod: last_

	method Action _ready(bit _is_ready);
		wr_in_write_data_ddr_ready <= _is_ready;
	endmethod: _ready
endinterface: ifc_write_data_ddr


/* Interface to send write request to the Xilinx AXI Datamover IP. */
interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
	method bit valid_();
		return rg_out_write_cmd_ddr_valid;
	endmethod: valid_

	method Bit#(72) data_();
		return {
			4'b0,  // reserved
			4'b0,  // command tag; not used
			// start address, 0'h01000000 is base address of DDR
			pack(rg_out_ddr_write_addr + 'h01000000),
			1'b0,  // DRE ReAlignment Request; not used
			1'b1,  // end of frame
			6'b0,  // DRE Stream Alignment; not used
			1'b1,  // INCR access type
			pack(rg_out_ddr_write_length)  // bytes to transfer
		};
	endmethod: data_

	method Action _ready(bit _is_ready);
		wr_in_write_cmd_ddr_ready <= _is_ready;
	endmethod: _ready
endinterface: ifc_write_cmd_ddr

interface Ifc_write_sts_ddr ifc_write_sts_ddr;
	method bit ready_();
		return 1'b1; //rg_out_write_sts_ddr_ready;
	endmethod: ready_

	method Action _valid(bit _is_valid);
		//wr_in_write_sts_ddr_valid <= _is_valid;
		if (_is_valid == 1'b1)
			rg_out_leds <= 'hF5;
	endmethod: _valid

	method Action _data_in(Bit#(8) _data);
		wr_in_write_sts_ddr_data <= _data;
	endmethod: _data_in
endinterface: ifc_write_sts_ddr

method Action _reg_in(Bit#(32) _data);
	if (_data == rg_last_data + 1)
		drg_restart <= True;
	else if (_data != rg_last_data)
		rg_address <= _data;
	rg_last_data <= _data;
endmethod: _reg_in

method Bit#(32) reg_out_();
	return rg_out_reg;
endmethod: reg_out_

method Bit#(8) leds_();
	return rg_out_leds;
endmethod: leds_

endmodule: mkTb_for_ddr_connection
endpackage: tb_ddr_connection

