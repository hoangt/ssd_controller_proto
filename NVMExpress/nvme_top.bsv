/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

////////////////////////////////////////////////////////////////////////////////
// Name of the Module   : NVM controller top
// Coded by             : M S Santosh Kumar
//
// Module Description	: This module has two modes controlled by 
//                        ENABLE_AXI_STREAM parameter. 
//                        1) When undefined, connects PCIe controller, NvmController
//                           and nand flash model in data path.
//                        2) When defined, NvmController nand flash interface is
//                           exposed onto top through AXI4 Stream using AXI4 wrapper
//                           defined in nvme_ddr_connection.bsv
//                                   
//////////////////////////////////////////////////////////////////////////////////


package nvme_top;

`include "global_parameters"
import Vector::*;
import NvmController::*;
import NvmWrapper::*;
import nvme_ddr_connection::*;
import global_definitions::*;
import NvmWrapperHeader::*;
import nand_flash_model::*;

// `define ENABLE_AXI_STREAM

`ifdef ENABLE_AXI_STREAM
interface Ifc_pcie_axi_stream;
   /* axi stream interfaces */
   interface Ifc_read_data_ddr ifc_read_data_ddr;
   interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
   interface Ifc_write_data_ddr ifc_write_data_ddr;
   interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
   /* pcie core interfaces */
   interface Ifc_tx_packet ifc_tx_packet;
   interface Ifc_rx_packet ifc_rx_packet;
   interface Ifc_config_space ifc_config_space;
endinterface
`else
interface Ifc_pcie;
   interface Ifc_tx_packet ifc_tx_packet;
   interface Ifc_rx_packet ifc_rx_packet;
   interface Ifc_config_space ifc_config_space;
endinterface
`endif

(*synthesize*)
(*always_ready,always_enabled*)
`ifdef ENABLE_AXI_STREAM
module mkNvmeTop(Ifc_pcie_axi_stream);
`else
module mkNvmeTop(Ifc_pcie);
`endif
   
   /* instantiate nvme controller */
   
   Ifc_nvme_wrapper lv_nvm_controller <- mkNvmWithWrapper;
   let lv_nvm_nand_flash_ifc = lv_nvm_controller.ifc_nand_flash;

   `ifdef ENABLE_AXI_STREAM

   /* Instantiate AXI stream bridge */
   Ifc_ddr_connection ifc_ddr_connection <- mkNvmDDRConnection;
   let lv_ddr_nand_flash_ifc = ifc_ddr_connection.ifc_nand_flash;
   
   rule rl_connect_nand_request_data;
      let lv_tmp <- lv_nvm_nand_flash_ifc[0].request_address_();
      lv_ddr_nand_flash_ifc._request_data(
	 truncate(lv_tmp.address),
	 unpack(truncate(lv_tmp.length))
	 );
   endrule: rl_connect_nand_request_data
   
   rule rl_connect_nand_data_in;
      let lv_tmp <- lv_ddr_nand_flash_ifc._get_data_();
      lv_nvm_nand_flash_ifc[0]._data_in(lv_tmp);
   endrule: rl_connect_nand_data_in
   
   rule rl_connect_nand_write;
      $display("%d: Testbench: nand flash write.", $stime());
      let lv_tmp <- lv_nvm_nand_flash_ifc[0].data_out_();
      lv_ddr_nand_flash_ifc._write(
	 truncate(lv_tmp.address),
	 lv_tmp.data,
	 truncate(lv_tmp.length)
	 );
   endrule: rl_connect_nand_write
   
   rule rl_connect_enable;
      lv_ddr_nand_flash_ifc._enable(~lv_nvm_nand_flash_ifc[0]._enable());
   endrule: rl_connect_enable
   
   rule rl_connect_interrupt;
      if (lv_ddr_nand_flash_ifc.interrupt_() == 1'b1)
	 lv_nvm_nand_flash_ifc[0]._interrupt();
   endrule: rl_connect_interrupt
   
   rule rl_connect_busy;
      lv_nvm_nand_flash_ifc[0]._busy(lv_ddr_nand_flash_ifc.busy_());
   endrule: rl_connect_busy
   
   interface ifc_write_cmd_ddr = ifc_ddr_connection.ifc_write_cmd_ddr;
   interface ifc_write_data_ddr = ifc_ddr_connection.ifc_write_data_ddr;
   interface ifc_read_cmd_ddr = ifc_ddr_connection.ifc_read_cmd_ddr;
   interface ifc_read_data_ddr = ifc_ddr_connection.ifc_read_data_ddr;
   
   `else

   /* Instantiate nand_flash_model */
   
   Vector#(`NO_CHANNELS, Ifc_nand_flash_model) lv_nand_flash_model;
   for (Integer i = 0; i < `NO_CHANNELS; i = i + 1) begin
      lv_nand_flash_model[i] <- mkNand_flash_model();
   end
   
   /* Make connection between lv_nand_flash_model and lv_nvm_nand_flash_ifc */
   for (Integer i = 0; i < `NO_CHANNELS; i = i + 1)
      begin
	 rule rl_connect_nand_request_data;
	    let lv_tmp <- lv_nvm_nand_flash_ifc[i].request_address_();
	    lv_nand_flash_model[i]._request_data(
	       truncate(lv_tmp.address),
	       unpack(truncate(lv_tmp.length))
	       );
	 endrule: rl_connect_nand_request_data
	 
	 rule rl_connect_nand_data_in;
	    let lv_tmp <- lv_nand_flash_model[i]._get_data_();
	    lv_nvm_nand_flash_ifc[i]._data_in(lv_tmp);
	 endrule: rl_connect_nand_data_in
	 
	 rule rl_connect_nand_write;
	    $display("%d: Testbench: nand flash write.", $stime());
	    let lv_tmp <- lv_nvm_nand_flash_ifc[i].data_out_();
	    lv_nand_flash_model[i]._write(
	       truncate(lv_tmp.address),
	       lv_tmp.data,
	       truncate(lv_tmp.length)
	       );
	 endrule: rl_connect_nand_write
	 
	 rule rl_connect_enable;
	    lv_nand_flash_model[i]._enable(~lv_nvm_nand_flash_ifc[i]._enable());
	 endrule: rl_connect_enable
      
	 rule rl_connect_interrupt;
	    if (lv_nand_flash_model[i].interrupt_() == 1'b1)
	       lv_nvm_nand_flash_ifc[i]._interrupt();
	 endrule: rl_connect_interrupt
	 
	 rule rl_connect_busy;
	    lv_nvm_nand_flash_ifc[i]._busy(lv_nand_flash_model[i].busy_());
	 endrule: rl_connect_busy
	 
	 rule rl_connect_write_ack;
	    let lv_write_status <- lv_nand_flash_model[i].write_failed();
	    lv_nvm_nand_flash_ifc[i]._write_status(lv_write_status);
	 endrule: rl_connect_write_ack

      end

   `endif
   interface ifc_tx_packet = lv_nvm_controller.ifc_tx_packet;
   interface ifc_rx_packet = lv_nvm_controller.ifc_rx_packet;
   interface ifc_config_space = lv_nvm_controller.ifc_config_space;
   
endmodule
   
endpackage
