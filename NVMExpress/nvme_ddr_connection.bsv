/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Copyright (c) 2013,2014  Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
////////////////////////////////////////////////////////////////////////////////
// Name of the Module : NVMe DDR connection
// Coded by           : M S Santosh Kumar
//
// Module Description : This module provides a wrapper across the NvmController
//                      nand flash interface and exposes AXI4 stream interface,
//                      used for DDR3 connection.
//
////////////////////////////////////////////////////////////////////////////////

`include "global_parameters"
package nvme_ddr_connection;

import DReg::*;
import global_definitions::*; 
import ConfigReg::*;

interface Ifc_ddr_connection;
   interface Ifc_nand_flash_model ifc_nand_flash;
   interface Ifc_read_data_ddr ifc_read_data_ddr;
   interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
   interface Ifc_write_data_ddr ifc_write_data_ddr;
   interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
endinterface

module mkNvmDDRConnection (Ifc_ddr_connection);

   /* Reg and Wires for nand flash model interface */
   Reg#(bit) rg_in_enable <- mkReg(0);
   Reg#(UInt#(21)) rg_write_count <- mkReg(0);
   
   /* Reg and Wires of Read command interface of AXI Data Mover */
   Reg#(bit) rg_out_read_cmd_ddr_valid <- mkReg(0);
   Wire#(bit) wr_in_read_cmd_ddr_ready <- mkReg(0);
   Reg#(UInt#(32)) rg_out_ddr_read_addr <- mkReg(0);
   Reg#(UInt#(23)) rg_out_ddr_read_length <- mkReg(0);
   
   /* Reg and Wires of Read data interface of AXI Data Mover */
   Wire#(bit) wr_in_read_data_ddr_valid <- mkWire();
   Wire#(Bit#(`WDC)) wr_in_read_data_ddr_data <- mkWire();
   Wire#(bit) wr_in_read_data_ddr_last <- mkWire();
   Wire#(bit) dwr_out_read_data_ready <- mkDWire(1'b0);
   
   /* Reg and Wires of write cmd interface of AXI Data Mover */
   Reg#(bit) rg_out_write_cmd_ddr_valid <- mkReg(0);
   Wire#(bit) wr_in_write_cmd_ddr_ready <- mkWire();
   Reg#(UInt#(32)) rg_out_ddr_write_addr <- mkReg(0);
   Reg#(UInt#(23)) rg_out_ddr_write_length <- mkReg(0);
   
   /* Reg and Wires of write data interface of AXI Data Mover */
   Reg#(bit) rg_out_write_data_ddr_valid <- mkReg(0);
   Wire#(bit) wr_in_write_data_ddr_ready <- mkWire();
   Reg#(bit) rg_out_write_data_ddr_last <- mkReg(0);
   Reg#(Bit#(`WDC)) rg_out_write_data_ddr_data <- mkReg(0);
   
   
   /* local Reg and Wires for ddr write */
   Reg#(Bool) rg_write_cmd_done <- mkConfigReg(True);
   Reg#(Bool) rg_write_data_done <- mkConfigReg(True);
   Wire#(Bool) dwr_write_method_fired <- mkDWire(False);
   Wire#(UInt#(64)) wr_write_addr <- mkWire();
   Wire#(Bit#(`WDC)) wr_write_data <- mkWire();
   Wire#(UInt#(11)) wr_write_length <- mkWire();
   
   /* local Reg and Wires for ddr read */
   Reg#(Bool) rg_ddr_read_in_progress <- mkReg(False);
   
   // rule_invalidate_write_data will fire only if the method _write doesn't fire
   // because method has more priority than rule where as rl_write_data fires only when
   // method _write fires implicitly and hence they are exclusive and same goes for 
   // other attributes.
   
   
   let lv_ddr_write_in_progress = (!rg_write_cmd_done) || (!rg_write_data_done); 

   (*mutually_exclusive = "rl_invalidate_write_data,rl_write_data"*)
   (*mutually_exclusive = "rl_invalidate_write_data,rl_write_cmd_and_first_data_word"*)

   rule rl_invalidate_read_cmd (rg_ddr_read_in_progress &&
      wr_in_read_cmd_ddr_ready == 1'b1);
      
      rg_out_read_cmd_ddr_valid <= 1'b0;
      
   endrule : rl_invalidate_read_cmd
      
   rule rl_invalidate_write_cmd (lv_ddr_write_in_progress && 
      wr_in_write_cmd_ddr_ready == 1'b1);
      
      rg_out_write_cmd_ddr_valid <= 1'b0;
      rg_write_cmd_done <= True;
      
   endrule : rl_invalidate_write_cmd
   
   rule rl_invalidate_write_data (!dwr_write_method_fired && wr_in_write_data_ddr_ready == 1'b1);
      
      rg_out_write_data_ddr_valid <= 1'b0;
      rg_out_write_data_ddr_last <= 1'b0;
      
   endrule : rl_invalidate_write_data
   
   rule rl_write_cmd_and_first_data_word (!lv_ddr_write_in_progress);
      
      // write command
      rg_out_write_cmd_ddr_valid <= 1'b1;
      rg_out_ddr_write_addr <= truncate(wr_write_addr)<<12;
      rg_out_ddr_write_length <= extend(wr_write_length)<<12;
      rg_write_data_done <= False;
      rg_write_cmd_done <= False;
      
      // write first data word
      rg_out_write_data_ddr_data <= wr_write_data;
      rg_out_write_data_ddr_valid <= 1'b1;
      rg_write_count <= rg_write_count + 1;
      rg_out_write_data_ddr_last <= 1'b0;
      
   endrule : rl_write_cmd_and_first_data_word
   
   rule rl_write_data (lv_ddr_write_in_progress);
      
      if (rg_write_count == (extend(wr_write_length)<<8) -1 )
	 begin
	    rg_write_count <= 0;
	    rg_out_write_data_ddr_last <= 1'b1;
	    rg_write_data_done <= True;
	 end
      else
	 begin
	    rg_out_write_data_ddr_last <= 1'b0;
	    rg_write_count <= rg_write_count + 1;
	 end
      
      rg_out_write_data_ddr_data <= wr_write_data;
      rg_out_write_data_ddr_valid <= 1'b1;
      
   endrule :rl_write_data
   
   // nand flash model interface
   interface Ifc_nand_flash_model ifc_nand_flash;
   
      // method to request data from ddr
      method Action _request_data(UInt#(64) _address, UInt#(11) _length) 
	 if (rg_in_enable == 1'b1 && !rg_ddr_read_in_progress);
      
	 rg_out_ddr_read_addr <= truncate(_address) << 12;
	 rg_out_ddr_read_length <= extend(_length) << 12;
	 rg_out_read_cmd_ddr_valid <= 1'b1;
	 rg_ddr_read_in_progress <= True;

      endmethod
   
      // method to get data from ddr
      method ActionValue#(Bit#(`WDC)) _get_data_() if (rg_in_enable == 1'b1 &&
	 wr_in_read_data_ddr_valid == 1'b1);
	 
	 dwr_out_read_data_ready <= 1'b1;
	 if (wr_in_read_data_ddr_last == 1'b1)
	    rg_ddr_read_in_progress <= False;
	 return wr_in_read_data_ddr_data;
   
      endmethod
   
      // method to write data to ddr
      method Action _write(UInt#(64) _address, Bit#(`WDC) _data,
			   UInt#(11) _length)
	 if (rg_in_enable == 1'b1 && (rg_out_write_data_ddr_valid == 1'b0 ||
				      wr_in_write_data_ddr_ready == 1'b1 ) &&
	     (rg_write_count != 0 || rg_write_cmd_done ));	 
      
	 dwr_write_method_fired <= True;
	 wr_write_data <= _data;
	 wr_write_addr <= _address;
	 wr_write_length <= _length;
   
      endmethod
   
      method Action _enable (bit _nand_ce_l);
	 rg_in_enable <= ~_nand_ce_l;
      endmethod
   
      method bit interrupt_() if (rg_in_enable == 1'b1);
	 return 1'b1;
      endmethod
   
      method bit busy_() if (rg_in_enable == 1'b1);
	 return 1'b0;
      endmethod
   
   endinterface
	    
   /* --- Interfaces for the connection to the Xilinx AXI Datamover IP. --- */
   /* Interface to read payload data from Xilinx AXI Datamover IP. */
   interface Ifc_read_data_ddr ifc_read_data_ddr;
      method bit ready_();
	 return dwr_out_read_data_ready;
      endmethod: ready_
   
      method Action _valid(bit _is_valid);
	 wr_in_read_data_ddr_valid <= _is_valid;
      endmethod: _valid
   
      method Action _data_in(Bit#(`WDC) _data);
	 wr_in_read_data_ddr_data <= _data;
      endmethod: _data_in
   
      method Action _last(bit _is_last);
	 wr_in_read_data_ddr_last <= _is_last;
	endmethod: _last
   endinterface: ifc_read_data_ddr
   

   /* Interface to send read request to the Xilinx AXI Datamover IP. */
   interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
      method bit valid_();
	 return rg_out_read_cmd_ddr_valid;
      endmethod: valid_
   
      method Bit#(72) data_();
	 return {
		 4'b0,  // reserved
		 4'b0,  // command tag; not used
		 // start address, 'h90000000 is base address of the DDR
		 pack(rg_out_ddr_read_addr + 'h90000000),
		 1'b0,  // DRE ReAlignment Request; not used
		 1'b1,  // end of frame
		 6'b0	,  // DRE Stream Alignment; not used
		 1'b1,  // INCR access type
		 pack(rg_out_ddr_read_length)  // bytes to transfer
		 };
      endmethod: data_

      method Action _ready(bit _is_ready);
	 wr_in_read_cmd_ddr_ready <= _is_ready;
      endmethod: _ready
   endinterface: ifc_read_cmd_ddr
   
   
   /* Interface to write payload data to the Xilinx AXI Datamover IP. */
   interface Ifc_write_data_ddr ifc_write_data_ddr;
      method bit valid_();
	 return rg_out_write_data_ddr_valid;
      endmethod: valid_
   
      method Bit#(`WDC) data_out_();
	 return rg_out_write_data_ddr_data;
      endmethod: data_out_
   
      method bit last_();
	 return rg_out_write_data_ddr_last;
      endmethod: last_
   
      method Action _ready(bit _is_ready);
	 wr_in_write_data_ddr_ready <= _is_ready;
      endmethod: _ready
   endinterface: ifc_write_data_ddr
   
   
   /* Interface to send write request to the Xilinx AXI Datamover IP. */
   interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
      method bit valid_();
	 return rg_out_write_cmd_ddr_valid;
      endmethod: valid_
   
      method Bit#(72) data_();
	 return {
		 4'b0,  // reserved
		 4'b0,  // command tag; not used
		 // start address, 0'h90000000 is base address of DDR
		 pack(rg_out_ddr_write_addr + 'h90000000),
		 1'b0,  // DRE ReAlignment Request; not used
		 1'b1,  // end of frame
		 6'b0,  // DRE Stream Alignment; not used
		 1'b1,  // INCR access type
		 pack(rg_out_ddr_write_length)  // bytes to transfer
		 };
      endmethod: data_
	    
      method Action _ready(bit _is_ready);
	 wr_in_write_cmd_ddr_ready <= _is_ready;
      endmethod: _ready
   endinterface: ifc_write_cmd_ddr
      
endmodule
endpackage
