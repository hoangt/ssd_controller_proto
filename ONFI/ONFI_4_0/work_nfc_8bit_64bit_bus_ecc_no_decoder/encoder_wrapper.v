/*
 * BCH Encoder wrapper Module
 */
//////`timescale 1ns / 1ps


module encoder_wrapper
		(
		input clk,
		input start,
		input ce,
		input [7:0] din,
		output [7:0] dout
		);

`include "bch_params.vh"

parameter T = 3;
parameter OPTION = "PARALLEL";
parameter DATA_BITS = 4096;
parameter BITS = 8;
parameter REG_RATIO = 1;

localparam BCH_PARAMS = bch_params(DATA_BITS, T);

`include "bch_defs.vh"

/* Generate code */
bch_encode #(BCH_PARAMS, BITS) u_bch_encode(
	.clk(clk),
	.start(start),
	.ce(ce),
	.data_in(din),
	.ready(),
	.data_out(dout),
	.data_bits(),
	.ecc_bits(),
	.first(),
	.last()
);
endmodule
