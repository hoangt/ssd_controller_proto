library verilog;
use verilog.vl_types.all;
entity mkTbNFC is
    port(
        CLK             : in     vl_logic;
        RST_N           : in     vl_logic
    );
end mkTbNFC;
