library verilog;
use verilog.vl_types.all;
entity mkNandFlashTarget is
    port(
        CLK             : in     vl_logic;
        RST_N           : in     vl_logic;
        \onfi_target_interface__data_to_nfc_m\: out    vl_logic_vector(7 downto 0);
        \RDY_onfi_target_interface__data_to_nfc_m\: out    vl_logic;
        \onfi_target_interface__data_from_nfc_m__data_from_nfc\: in     vl_logic_vector(7 downto 0);
        \EN_onfi_target_interface__data_from_nfc_m\: in     vl_logic;
        \RDY_onfi_target_interface__data_from_nfc_m\: out    vl_logic;
        \onfi_target_interface__onfi_ce_n_m__onfi_ce_n\: in     vl_logic;
        \EN_onfi_target_interface__onfi_ce_n_m\: in     vl_logic;
        \RDY_onfi_target_interface__onfi_ce_n_m\: out    vl_logic;
        \onfi_target_interface__onfi_we_n_m__onfi_we_n\: in     vl_logic;
        \EN_onfi_target_interface__onfi_we_n_m\: in     vl_logic;
        \RDY_onfi_target_interface__onfi_we_n_m\: out    vl_logic;
        \onfi_target_interface__onfi_re_n_m__onfi_re_n\: in     vl_logic;
        \EN_onfi_target_interface__onfi_re_n_m\: in     vl_logic;
        \RDY_onfi_target_interface__onfi_re_n_m\: out    vl_logic;
        \onfi_target_interface__onfi_wp_n_m__onfi_wp_n\: in     vl_logic;
        \EN_onfi_target_interface__onfi_wp_n_m\: in     vl_logic;
        \RDY_onfi_target_interface__onfi_wp_n_m\: out    vl_logic;
        \onfi_target_interface__onfi_cle_m__onfi_cle\: in     vl_logic;
        \EN_onfi_target_interface__onfi_cle_m\: in     vl_logic;
        \RDY_onfi_target_interface__onfi_cle_m\: out    vl_logic;
        \onfi_target_interface__onfi_ale_m__onfi_ale\: in     vl_logic;
        \EN_onfi_target_interface__onfi_ale_m\: in     vl_logic;
        \RDY_onfi_target_interface__onfi_ale_m\: out    vl_logic;
        \onfi_target_interface_t_ready_busy_n_\: out    vl_logic;
        \RDY_onfi_target_interface_t_ready_busy_n_\: out    vl_logic
    );
end mkNandFlashTarget;
