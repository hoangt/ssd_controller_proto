library verilog;
use verilog.vl_types.all;
entity BRAM2Load is
    generic(
        FILENAME        : string  := "";
        PIPELINED       : integer := 0;
        ADDR_WIDTH      : integer := 1;
        DATA_WIDTH      : integer := 1;
        MEMSIZE         : integer := 1;
        BINARY          : integer := 0
    );
    port(
        CLKA            : in     vl_logic;
        ENA             : in     vl_logic;
        WEA             : in     vl_logic;
        ADDRA           : in     vl_logic_vector;
        DIA             : in     vl_logic_vector;
        DOA             : out    vl_logic_vector;
        CLKB            : in     vl_logic;
        ENB             : in     vl_logic;
        WEB             : in     vl_logic;
        ADDRB           : in     vl_logic_vector;
        DIB             : in     vl_logic_vector;
        DOB             : out    vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of FILENAME : constant is 1;
    attribute mti_svvh_generic_type of PIPELINED : constant is 1;
    attribute mti_svvh_generic_type of ADDR_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of DATA_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of MEMSIZE : constant is 1;
    attribute mti_svvh_generic_type of BINARY : constant is 1;
end BRAM2Load;
