library verilog;
use verilog.vl_types.all;
entity encoder_wrapper is
    generic(
        T               : integer := 3;
        OPTION          : string  := "PARALLEL";
        DATA_BITS       : integer := 4096;
        BITS            : integer := 8;
        REG_RATIO       : integer := 1
    );
    port(
        clk             : in     vl_logic;
        start           : in     vl_logic;
        ce              : in     vl_logic;
        din             : in     vl_logic_vector(7 downto 0);
        dout            : out    vl_logic_vector(7 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of T : constant is 1;
    attribute mti_svvh_generic_type of OPTION : constant is 1;
    attribute mti_svvh_generic_type of DATA_BITS : constant is 1;
    attribute mti_svvh_generic_type of BITS : constant is 1;
    attribute mti_svvh_generic_type of REG_RATIO : constant is 1;
end encoder_wrapper;
