library verilog;
use verilog.vl_types.all;
entity SizedFIFO is
    generic(
        p1width         : integer := 1;
        p2depth         : integer := 3;
        p3cntr_width    : integer := 1;
        \guarded\       : integer := 1
    );
    port(
        CLK             : in     vl_logic;
        RST             : in     vl_logic;
        D_IN            : in     vl_logic_vector;
        ENQ             : in     vl_logic;
        FULL_N          : out    vl_logic;
        D_OUT           : out    vl_logic_vector;
        DEQ             : in     vl_logic;
        EMPTY_N         : out    vl_logic;
        CLR             : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of p1width : constant is 1;
    attribute mti_svvh_generic_type of p2depth : constant is 1;
    attribute mti_svvh_generic_type of p3cntr_width : constant is 1;
    attribute mti_svvh_generic_type of \guarded\ : constant is 1;
end SizedFIFO;
