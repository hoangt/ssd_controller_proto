library verilog;
use verilog.vl_types.all;
entity BRAM1 is
    generic(
        PIPELINED       : integer := 0;
        ADDR_WIDTH      : integer := 1;
        DATA_WIDTH      : integer := 1;
        MEMSIZE         : integer := 1
    );
    port(
        CLK             : in     vl_logic;
        EN              : in     vl_logic;
        WE              : in     vl_logic;
        ADDR            : in     vl_logic_vector;
        DI              : in     vl_logic_vector;
        DO              : out    vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of PIPELINED : constant is 1;
    attribute mti_svvh_generic_type of ADDR_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of DATA_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of MEMSIZE : constant is 1;
end BRAM1;
