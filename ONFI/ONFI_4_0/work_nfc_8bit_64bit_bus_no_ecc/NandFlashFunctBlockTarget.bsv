/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Copyright (c) 2014  Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

////////////////////////////////////////////////////////////////////////////////
// Name of the Module	: ONFI 4.0 Standard + emulating flash target with specifications described below.
// Coded by				: Laxmeesha.S
//
// Module Description	: This module contains 1. ONFI 4.0 standard with following operations
//							a. RESET flash
//							b. READ PAGE FROM TARGET flash
//							c. READ PAGE FROM TARGET flash multi-plane concurrency
//							d. READ PAGE FROM TARGET flash multi-LUN concurrency
//							e. READ PAGE sequential/random FROM TARGET (also called READ PAGE CACHE operation)
//							f. PROGRAM PAGE in TARGET flash
//							g. PROGRAM PAGE in TARGET flash multi-plane concurrency
//							h. PROGRAM PAGE in TARGET flash multi-LUN concurrency
//							i. PROGRAM PAGE sequential/random in TARGET (also called PROGRAM PAGE CACHE operation)
//							j. BLOCK ERASE 
//							k. BLOCK ERASE multi-plane concurrency
//							l. BLOCK ERASE multi-LUN concurrency
//							m. CHANGE READ COLUMN (to change column address inside page)
//						        n. READ ID
//						        o. READ PARAMETER PAGE
//						        p. READ UNIQUE ID
//						        q. GET/READ FEATURES
//						        r. SET/WRITE FEATURES
//						        s. READ STATUS
//						        t. READ STATUS FROM SELECTED LUN (also called SELECT LUN WITH STATUS)
//                                             2. Target flash memory with 
//							a. 2 LUNS(fixed)
//							b. 2 PLANES per LUN(fixed)
//							c. 1024 BLOCKS per PLANE (for simulating modelling only 4 BLOCKS per PLANE)(parameterized)
//							d. 64 PAGES per BLOCK (for simulating modelling only 8 PAGES per BLOCK)(parameterized)
//							e. (4k+128) bytes memory per PAGE. (64 bytes for ECC. Total 8K bytes present)(parameterized)
//							f. 8 bit Data bus
////////////////////////////////////////////////////////////////////////////////
/* global parameter definitions */
`include "global_parameters_Flash.bsv"

package NandFlashFunctBlockTarget ;

import Vector::*;
import BRAM :: * ;
import InterfaceNandFlashFunctBlockTarget :: * ;

interface ONFI_Target_Interface ;
	interface TargetInterface onfi_target_interface ;
endinterface


//Below are the possible operation states of block
typedef enum {
	IDLE ,
	RESET ,
	READ ,
	READ_PAGE_CACHE ,
	READ_PAGE_CACHE_LAST ,
	READ_CACHE_REG ,
	READ_STATUS ,
	READ_ID ,
	READ_PARAM ,
	READ_UID ,
	READ_FEATURE ,
	SET_FEATURE ,
	PROGRAM ,
	PROGRAM_PAGE_CACHE ,
	PROGRAM_PAGE_CACHE_LAST ,
	PRGM_CACHE_REG ,
	ERASE 
} Operation_state deriving( Bits, Eq ) ;

//Below states are needed for reading/writing parameters/features/id's from/to the device.
typedef enum {
	READ ,
	READ_1 ,
	WAIT 
} Read_feature_state deriving( Bits, Eq ) ;
typedef enum {
	WRITE ,
	WRITE_1 
} Write_feature_state deriving( Bits, Eq ) ;

//Below enums are used for forming state transitions during adddress cycles.
typedef enum {
	COL_ADDR1 ,
	COL_ADDR2 ,
	PAGE_ADDR ,
	BLOCK_ADDR ,
	LUN_ADDR
} Addr_cycle_state deriving( Bits, Eq ) ;

(*synthesize*)
module mkNandFlashTarget ( ONFI_Target_Interface ) ;

// Regs and Wires for functional block to control target
	Reg#(Bit#(8))		rg_data_to_nfc	  <- mkReg(0) ;	  // data out
	Wire#(Bit#(8))		wr_data_from_nfc  <- mkWire ; // data in
	Wire#(bit)		wr_onfi_ce_n	  <- mkWire ; // active low
	Wire#(bit)		wr_onfi_we_n	  <- mkWire ; // active low
	Wire#(bit)		wr_onfi_re_n	  <- mkWire ; // active low
	Wire#(bit)		wr_onfi_wp_n	  <- mkWire ; // active low
	Wire#(bit)		wr_onfi_cle	  <- mkWire ; // active high
	Wire#(bit)		wr_onfi_ale	  <- mkWire ; // active high
	Reg#(bit)		rg_t_ready_busy_n <- mkReg(1) ;   // active low
	
// Other registers needed.
	Reg#(Bit#(8))	                          command_register  <- mkReg(0) ;	
	Reg#(Bit#(`PAGE_WIDTH))                 page_addr_register  <- mkReg(0) ;
	Reg#(Bit#(`BLOCK_WIDTH))               block_addr_register  <- mkReg(0) ;
	Reg#(Bit#(8))	                   id_config_addr_register  <- mkReg(0) ; 
	Vector#(`TOTAL_PLANE,Reg#(Bit#(1)))             flag_FAILC  <- replicateM(mkReg(1'b0)); // This is to update bit 1 of the SR,1 each bit for every plane in every target
	Vector#(`TOTAL_PLANE,Reg#(Bit#(1)))	         flag_FAIL  <- replicateM(mkReg(1'b0)); // This is to update bit 0 of the SR,1 each bit for every plane in every target
	Vector#(`TOTAL_PLANE,Reg#(Bit#(1)))	          flag_RDY  <- replicateM(mkReg(1'b1)); // This is to update bit 6 of the SR,1 each bit for every plane in every target
	Vector#(`TOTAL_PLANE,Reg#(Bit#(1)))	         flag_ARDY  <- replicateM(mkReg(1'b1)); // This is to update bit 7 of the SR,1 each bit for every plane in every target
	Reg#(Operation_state)                   rg_op_state_global  <- mkReg(IDLE);	        // This is to keep track of the global opeartion irrespective of LUNS.
	Reg#(Bit#(`LUN_WIDTH)) 	  	                   reg_lun  <- mkReg(0) ;
	Reg#(Bit#(`PLANE_WIDTH))                         reg_plane  <- mkReg(0) ;
	Vector#(`TOTAL_PLANE,Reg#(Bit#(1))) flag_read_queued_plane  <- replicateM(mkReg(0)) ;    // In case of Multi-plane read,to id queued planes which are queued to be read
	Vector#(`TOTAL_PLANE,Reg#(Bit#(1))) flag_prgm_queued_plane  <- replicateM(mkReg(0)) ;    // In case of Multi-plane prgm,to id queued planes which are queued to be prgmed
	Vector#(`TOTAL_PLANE,Reg#(Bit#(1)))         flag_prgm_last  <- replicateM(mkReg(0)) ;    // In case of prgm page cache,to id queued planes which are queued to be prgmed
	Vector#(`TOTAL_PLANE,Reg#(Bit#(1))) flag_erase_queued_plane <- replicateM(mkReg(0)) ;    // In case of Multi-plane eras,to id queued planes which are queued to be erased
	Vector#(`TOTAL_PLANE,Reg#(Bit#(`BLOCK_WIDTH))) queued_block <- replicateM(mkReg('h0));   // In case of Multi-plane read,block addresses of the queued blocks to be read
	Vector#(`TOTAL_PLANE,Reg#(Bit#(`PAGE_WIDTH)))   queued_page <- replicateM(mkReg('h0));  // In case of Multi-plane read,page addrs of the queued pages to be read in block
        Reg#(Bit#(TAdd#(`COLUMN_WIDTH,1)))       col_addr_register  <- mkReg(0) ;
	Reg#(Bit#(TLog#(`TOTAL_PLANE)))            cache_reg_select <- mkReg(0) ;		 // This is to select which cache reg is in use by the NFC.
	Reg#(Addr_cycle_state)                 rg_status_addr_state <- mkReg(PAGE_ADDR) ;	 // For status read operation , addr cycke states.
	Reg#(Addr_cycle_state)                   rg_read_addr_state <- mkReg(COL_ADDR1) ;	 // For read operation , addr cycke states.
	Vector#(TExp#(`LUN_WIDTH),Reg#(Bit#(1)))     flag_page_read <- replicateM(mkReg(0));     // Needed to distinguish b/t read_page and read_mode commands.
Vector#(TExp#(`LUN_WIDTH),Reg#(Bit#(1)))   wait_program_page_cache  <- replicateM(mkReg(0));     // Flag to see if program page cache and page cache last overlaps.
Vector#(TExp#(`LUN_WIDTH),Reg#(Bit#(1)))      wait_read_page_cache  <- replicateM(mkReg(0)) ;    // In case of read page cache, need to track 1st or last read cache op.
Vector#(TExp#(`LUN_WIDTH),Reg#(Operation_state))       rg_op_state  <- replicateM(mkReg(IDLE)); // One reg_op_state flag for each LUN, since b/t LUNS concurrency exists. 
Vector#(`TOTAL_PLANE,Reg#(Bit#(`BLOCK_WIDTH)))    queued_block_last <- replicateM(mkReg('h0));   // Back up block addr copy for program page cache last
Vector#(`TOTAL_PLANE,Reg#(Bit#(`PAGE_WIDTH)))      queued_page_last <- replicateM(mkReg('h0));   // Back up page addr copy for program page cache last
Vector#(`TOTAL_PLANE,Reg#(Bit#(1)))    flag_prgm_queued_plane_last  <- replicateM(mkReg(0)) ;    // Needed in case of overlap between program page cache and page cache last. 
Vector#(`TOTAL_PLANE,Reg#(Bit#(1)))    flag_read_queued_plane_last  <- replicateM(mkReg(0)) ;    // Needed in case of overlap between read page cache and cahce last.
Vector#(`TOTAL_PLANE,Reg#(Bit#(TAdd#(`COLUMN_WIDTH,1)))) target_col_address <- replicateM(mkReg(0)) ; //This to track and count col/col offset address in target.
Vector#(`TOTAL_PLANE,Reg#(Bit#(TAdd#(`COLUMN_WIDTH,1))))   read_reg_address <- replicateM(mkReg(0)) ; // This is to keep track of col offset in cache registers.
	
	//Creating cache register for every plane of every LUN
	Vector#(`TOTAL_PLANE, BRAM_Configure) cfg_cache_reg; 
	for(Integer i = 0; i < `TOTAL_PLANE ; i = i + 1) begin 
      		cfg_cache_reg[i] = defaultValue; 
   	end
	Vector#(`TOTAL_PLANE, BRAM2Port#(Bit#(TAdd#(`COLUMN_WIDTH,1)), Bit#(8))) cache_reg <- mapM(mkBRAM2Server, cfg_cache_reg);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////          HERE WE ARE DEFINING THE DATA REGISTER PER PLANE AND ID MEMORY, FEATURE MEMORY, UID MEMORY, PARAMETER MEMORY                                      //////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//Creating data register for every plane of every LUN
	Vector#(`TOTAL_PLANE, BRAM_Configure) cfg_data_reg; 
	for(Integer i = 0; i < `TOTAL_PLANE ; i = i + 1) begin 
      		cfg_data_reg[i] = defaultValue; 
   	end
	Vector#(`TOTAL_PLANE, BRAM2Port#(Bit#(TAdd#(`COLUMN_WIDTH,1)), Bit#(8))) data_reg <- mapM(mkBRAM2Server, cfg_data_reg);	
	
	//Creating memory for ID parameters 
	BRAM_Configure cfg_id_memory                      = defaultValue ;
	cfg_id_memory.loadFormat = tagged Hex "id.txt" ;
	BRAM2Port#( Bit#(4) , Bit#(8) ) id_memory        <- mkBRAM2Server (cfg_id_memory) ; // Address lines = 4 bits ,Total 10 bytes data

	//Creating memory for parameter page 
	BRAM_Configure cfg_param_memory                   = defaultValue ;
	cfg_param_memory.loadFormat = tagged Hex "parameter_page.txt" ;
	BRAM2Port#( Bit#(8) , Bit#(8) ) param_memory     <- mkBRAM2Server (cfg_param_memory) ; // Address lines = 8 bits ,Total 256 bytes data
	
	//Creating memory for unique ID parameters
	BRAM_Configure cfg_uid_memory                     = defaultValue ;
	cfg_uid_memory.loadFormat = tagged Hex "uid.txt" ;
	BRAM2Port#( Bit#(5) , Bit#(8) )  uid_memory      <- mkBRAM2Server (cfg_uid_memory) ; // Address lines = 5 bits ,Total 32 bytes data.Last 16B = invert(first 16B)
	
	//Creating memory for Feature parameters
	BRAM_Configure cfg_feature_memory                 = defaultValue ;
	cfg_feature_memory.loadFormat = tagged Hex "feature.txt" ;
	BRAM2Port#( Bit#(8) , Bit#(32) )  feature_memory  <- mkBRAM2Server (cfg_feature_memory) ; // Address lines = 8 bits ,Total 4 bytes data per 8 bit address data.
	
	
	Reg#(Read_feature_state)  rg_read_state  <- mkReg(READ);
	Reg#(Write_feature_state) rg_write_state <- mkReg(WRITE);
	
	//Address counters for 4 planes
	Vector#(`TOTAL_PLANE,Reg#(Bit#(TAdd#(`COLUMN_WIDTH,1)))) cache_reg_address  <- replicateM(mkReg(0)) ;
	Vector#(`TOTAL_PLANE,Reg#(Bit#(TAdd#(`COLUMN_WIDTH,1))))  data_reg_address  <- replicateM(mkReg(0)) ;
		
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////               END OF DEFINING THE DATA REGISTER PER PLANE AND ID MEMORY, FEATURE MEMORY, UID MEMORY, PARAMETER MEMORY                                      //////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//(* mutually_exclusive = "rl_command_cycle_onfi, rl_address_cycle_onfi, rl_data_write_cache_reg_from_nfc, rl_data_read_cache_reg_to_nfc, rl_read_id, rl_read_param, rl_read_uid, rl_read_feature, rl_set_feature, rl_read_status" *)

	/*rule rl_standby_onfi (wr_onfi_ce_n == 1'b1) ;
		$display (" CHIP ENABLE PIN IS DISABLED , TARGET IN STANDBY\n")   ;
		//Here can cut off power for power save
	endrule*/
	
	//Command cycle will fire only if NFC enables CLE.
	rule rl_command_cycle_onfi (wr_onfi_ce_n == 1'b0 && wr_onfi_we_n == 1'b0 && wr_onfi_ale == 1'b0 && wr_onfi_cle == 1'b1 && wr_onfi_re_n == 1'b1) ;
		command_register <= wr_data_from_nfc ;
		case (wr_data_from_nfc)
			'hFF                : 	begin
							col_addr_register      <= 'h0;
							page_addr_register     <= 'h0;
							block_addr_register    <= 'h0;
							rg_op_state_global     <= RESET ;
							for (Integer i = 0; i < (2**`LUN_WIDTH) ; i = i + 1)
								rg_op_state[i] <= IDLE ;
						end
			'h70 		    :	begin
							rg_op_state_global     <= READ_STATUS ;        
						end
			'hE0	            :   begin
			                                // Saving the cache register to be read , since reg-lun,plane changes if an operation is issued on the other LUN.
							cache_reg_select       <= {reg_lun,reg_plane} ;
							rg_op_state_global     <= READ_CACHE_REG ;// Can read only one cache reg to NFC at a time, hence global flag.
							//This is to keep track of col offset address for cache registers.
							//read_reg_address[{reg_lun,reg_plane}]   <= {'h0,col_addr_register} ;
							read_reg_address[{reg_lun,reg_plane}]   <= col_addr_register ;
						end
			'h30,'h35	    : 	begin                                                          //READ PAGE and COPYBACK READ are same.
							//Inherently assuming that NFC issues this operation after prev operation is completed in targeted plane.
							flag_read_queued_plane[{reg_lun,reg_plane}] <= 1'b1 ;
							target_col_address[{reg_lun,reg_plane}]     <= col_addr_register ;
							cache_reg_address[{reg_lun,reg_plane}]      <= col_addr_register ;
							//This is to keep track of col offset address for cache registers.
							read_reg_address[{reg_lun,reg_plane}]       <= col_addr_register ;
							queued_block[{reg_lun,reg_plane}]           <= block_addr_register ;
							queued_page[{reg_lun,reg_plane}]            <= page_addr_register ;
							rg_op_state[reg_lun]     <= READ ;  // Only selected lun in READ state, other LUN continues in prev state.
							//Below flag is needed to keep track, if status read is issued next,the next 00 command implies read mode not read page.
							flag_page_read[reg_lun]                     <= 1'b1 ; 
						end
			'h32                :	begin
							//Inherently assuming that NFC issues this operation after prev operation is completed in targeted plane.
							flag_read_queued_plane[{reg_lun,reg_plane}] <= 1'b1 ;
							target_col_address[{reg_lun,reg_plane}]     <= col_addr_register ;
							cache_reg_address[{reg_lun,reg_plane}]      <= col_addr_register ;
							//This is to keep track of col offset address for cache registers.
							read_reg_address[{reg_lun,reg_plane}]       <= col_addr_register ;
							queued_block[{reg_lun,reg_plane}]           <= block_addr_register ;
							queued_page[{reg_lun,reg_plane}]            <= page_addr_register ;
						end
// READ PAGE CACHE OPERATIONS (31,3F) cannot be interleaved b/t LUNS. i.e if LUN0 is issued a read page , then the read page sequential command must be issued before selecting the other LUN (LUN1) for any other operation. Similarly if LUN0 is issued a read page cache sequential/random command , then the read page last command cnnot be issued after selecting another LUN. This is because these operations must point to the same lun and plane and hence cannot be switched. Only solution is to first issue the operation is other LUN(lun1) and then when it is operating , can issue the read page cache commands in plane0 without any problem.
			'h31		    : 	begin
							if(command_register != 'h00) //then command issued is not 00-31. Hence read page cache sequential.
							begin
								//sequential page cache read. Hence calculate next page address.
								//if(page_addr_register == total_pages)
								//begin
									//Sequential page read cannot be done pointng to last page of the block
								//end
								//else
								//begin
									flag_read_queued_plane[{reg_lun,reg_plane}] <= 1'b1 ;
									target_col_address[{reg_lun,reg_plane}]     <= col_addr_register ;
									cache_reg_address[{reg_lun,reg_plane}]      <= col_addr_register ;
									//This is to keep track of col offset address for cache registers.
									read_reg_address[{reg_lun,reg_plane}]       <= col_addr_register ;
									queued_block[{reg_lun,reg_plane}]           <= block_addr_register ;
									queued_page[{reg_lun,reg_plane}]            <= page_addr_register + 'h1;
									page_addr_register	  		    <= page_addr_register + 'h1;
								//end   
							end
							else
							begin
								//random page cache read. Hence address already in the registers.
								flag_read_queued_plane[{reg_lun,reg_plane}] <= 1'b1 ;
								target_col_address[{reg_lun,reg_plane}]     <= col_addr_register ;
								cache_reg_address[{reg_lun,reg_plane}]      <= col_addr_register ;
								//This is to keep track of col offset address for cache registers.
								read_reg_address[{reg_lun,reg_plane}]       <= col_addr_register ;
								queued_block[{reg_lun,reg_plane}]           <= block_addr_register ;
								queued_page[{reg_lun,reg_plane}]            <= page_addr_register ;
							end
							flag_page_read[reg_lun]                     <= 1'b1 ;//Not sure abt this. Testing this.
//If we are doing 31h for first time , need to directly read from target page to data register (in which case wait flag will be 0). OTOH if we are doing 31h for 2nd or higher times, need to first read from data reg to cache reg and then only start reading from target page to data reg. Hence this case need read page cache last first.
							wait_read_page_cache[reg_lun] <= 1;
							if(wait_read_page_cache[reg_lun] == 0)
								rg_op_state[reg_lun] <= READ_PAGE_CACHE ;
							else
								rg_op_state[reg_lun] <= READ_PAGE_CACHE_LAST ;
						end
			'h3F                : 	begin
							//Need to read from data register.reg_lun and reg_plane will be already pointing to the data register needed.
							flag_read_queued_plane[{reg_lun,reg_plane}] <= 1'b1 ;
							rg_op_state[reg_lun]                        <= READ_PAGE_CACHE_LAST ;
							wait_read_page_cache[reg_lun]               <= 0; //Disable wait flag since no more cache read op.
							//Below flag is needed to keep track, if status read is issued next,the next 00 command implies read mode not read page.
							flag_page_read[reg_lun]                     <= 1'b1 ;
						end
			'h10                : 	begin
						        //Inherently assuming that NFC issues this operation after prev operation is completed in targeted plane.
						        flag_prgm_queued_plane[{reg_lun,reg_plane}] <= 1'b1 ;
							target_col_address[{reg_lun,reg_plane}]     <= col_addr_register ;
							cache_reg_address[{reg_lun,reg_plane}]      <= col_addr_register ;
							//This is to keep track of col offset address for cache registers.
							read_reg_address[{reg_lun,reg_plane}]       <= col_addr_register ;
						        queued_block[{reg_lun,reg_plane}]	    <= block_addr_register ;
						        queued_page[{reg_lun,reg_plane}] 	    <= page_addr_register ;
							rg_op_state[reg_lun]     <= PROGRAM ;  // Only selected lun in PROGRAM state, other LUN continues in prev state.
						end
			'h11                :   begin
							//Inherently assuming that NFC issues this operation after prev operation is completed in targeted plane.
							flag_prgm_queued_plane[{reg_lun,reg_plane}] <= 1'b1 ;
							target_col_address[{reg_lun,reg_plane}]     <= col_addr_register ;
							cache_reg_address[{reg_lun,reg_plane}]      <= col_addr_register ;
							//This is to keep track of col offset address for cache registers.
							read_reg_address[{reg_lun,reg_plane}]       <= col_addr_register ;
							queued_block[{reg_lun,reg_plane}]           <= block_addr_register ;
							queued_page[{reg_lun,reg_plane}]            <= page_addr_register ;
						end
			'h15                :	begin
							flag_prgm_queued_plane[{reg_lun,reg_plane}]  <= 1'b1 ;
							target_col_address[{reg_lun,reg_plane}]      <= col_addr_register ;
							cache_reg_address[{reg_lun,reg_plane}]       <= col_addr_register ;
							//This is to keep track of col offset address for cache registers.
							read_reg_address[{reg_lun,reg_plane}]        <= col_addr_register ;
							queued_block[{reg_lun,reg_plane}]            <= block_addr_register ;
							queued_page[{reg_lun,reg_plane}]             <= page_addr_register ;
//If already a data write is happening from data register to target(program page cache last) then the new program page cache needs to wait until it finishes.
							if(rg_op_state[reg_lun] == PROGRAM_PAGE_CACHE_LAST)
								wait_program_page_cache[reg_lun]     <= 1;
							else
								rg_op_state[reg_lun]                 <= PROGRAM_PAGE_CACHE ;
						end
			'hD0                :   begin 
							//Inherently assuming that NFC issues this operation after prev operation is completed in targeted plane.
							flag_erase_queued_plane[{reg_lun,reg_plane}] <= 1'b1 ;
							target_col_address[{reg_lun,reg_plane}]      <= col_addr_register ;
							cache_reg_address[{reg_lun,reg_plane}]       <= col_addr_register ;
							//This is to keep track of col offset address for cache registers.
							read_reg_address[{reg_lun,reg_plane}]        <= col_addr_register ;
							queued_block[{reg_lun,reg_plane}]            <= block_addr_register ;
							queued_page[{reg_lun,reg_plane}]             <= page_addr_register ;
							rg_op_state[reg_lun]     <= ERASE ;  
			                        end
			'hD1                :	begin
							//Inherently assuming that NFC issues this operation after prev operation is completed in targeted plane.
							flag_erase_queued_plane[{reg_lun,reg_plane}] <= 1'b1 ;
							target_col_address[{reg_lun,reg_plane}]      <= col_addr_register ;
							cache_reg_address[{reg_lun,reg_plane}]       <= col_addr_register ;
							//This is to keep track of col offset address for cache registers.
							read_reg_address[{reg_lun,reg_plane}]        <= col_addr_register ;
							queued_block[{reg_lun,reg_plane}]	     <= block_addr_register ;
							queued_page[{reg_lun,reg_plane}]	     <= page_addr_register ;
						end
			'h00                :   begin
/* Only if the operation READ PAGE(from target 00-30 or 00-35), is monitored by status read(70 or 78h), the next 00h command will result in read_mode(i.e read from cache), else
the 00h will be starting of another command set,Hence use the flag_page_read, which monitors if the status operation is used to monitor an earlier READ PAGE commmand*/ 
							if(rg_op_state_global == READ_STATUS && flag_page_read[reg_lun] == 1'b1)  
							begin
							// Saving the cache register to be read , since reg-lun,plane changes if an operation is issued on the other LUN.
								cache_reg_select        <= {reg_lun,reg_plane} ;
								rg_op_state_global      <= READ_CACHE_REG ;
								flag_page_read[reg_lun] <= 1'b0 ;
							end
							else
							begin
								rg_op_state_global <= IDLE ;
							end
						end					
			default             :	begin
							rg_op_state_global <= IDLE ;
						end
		endcase
	endrule
	
	Reg#(Bit#(8)) buff1 <- mkReg(0) ; 
	Reg#(Bit#(8)) buff2 <- mkReg(0) ; 
	Reg#(Bit#(8)) buff3 <- mkReg(0) ;
	Reg#(Bit#(8)) buff4 <- mkReg(0) ; 
	
	//Command cycle will fire only if NFC enables ALE.
	rule rl_address_cycle_onfi (wr_onfi_ce_n == 1'b0 && wr_onfi_we_n == 1'b0 && wr_onfi_ale == 1'b1 && wr_onfi_cle == 1'b0 && wr_onfi_re_n == 1'b1) ;
		case (command_register)
			'h90                : 	begin
							if(wr_data_from_nfc == 'h00)
							begin
								id_config_addr_register <= 'h0;
							end
							else
							begin
								id_config_addr_register <= 'h5;
							end
							rg_op_state_global              <= READ_ID ;
						end
			'hEC                :	begin
							id_config_addr_register <= 'h0;     //Read the whole param page from address 0. No other option provided.
							rg_op_state_global      <= READ_PARAM ;	
						end
			'hED		    :	begin
							id_config_addr_register <= 'h0;     //Read the whole param page from address 0. No other option provided.
							rg_op_state_global      <= READ_UID ;
						end
			'hEE		    :	begin
							id_config_addr_register <= wr_data_from_nfc;
							rg_op_state_global      <= READ_FEATURE ;
						end
			'hEF		    :	begin
							id_config_addr_register <= wr_data_from_nfc;
							rg_op_state_global      <= SET_FEATURE ;
						end
			'h78,'h60           :	begin
	case(rg_status_addr_state)
		PAGE_ADDR : begin
			    `ifdef PAGE_WIDTH_LT_8
		     		page_addr_register   <= wr_data_from_nfc[`PAGE_WIDTH-1:0] ;
		           	`ifdef PAGE_PLANE_WIDTH_LT_8
			 		reg_plane <= wr_data_from_nfc[`PAGE_WIDTH] ;
				`elsif PAGE_PLANE_WIDTH_E_8
					reg_plane <= wr_data_from_nfc[`PAGE_WIDTH] ;
				`endif
			    `elsif `define PAGE_WIDTH_E_8
				page_addr_register   <= wr_data_from_nfc[`PAGE_WIDTH-1:0] ;
		           	`ifdef PAGE_PLANE_WIDTH_LT_8
			 		reg_plane <= wr_data_from_nfc[`PAGE_WIDTH] ;
				`elsif PAGE_PLANE_WIDTH_E_8
					reg_plane <= wr_data_from_nfc[`PAGE_WIDTH] ;
				`endif
		  	    `else
		       		 buff3   <= wr_data_from_nfc ;
				 //page_addr_register[7:0]   <= wr_data_from_nfc ;
			     `endif
		
		  	    `ifdef PAGE_PLANE_BLOCK_WIDTH_LTE_8
		     		block_addr_register <= wr_data_from_nfc[`PAGE_WIDTH+`BLOCK_WIDTH+`PLANE_WIDTH-1:`PAGE_WIDTH+`PLANE_WIDTH];
			    `elsif PAGE_PLANE_BLOCK_WIDTH_GT_8
			    	`ifdef PAGE_PLANE_WIDTH_LT_8
					//block_addr_register[7-(`PAGE_WIDTH+`PLANE_WIDTH):0] <= wr_data_from_nfc[7:(`PAGE_WIDTH+`PLANE_WIDTH)];
			       		buff1 <= wr_data_from_nfc >> (`PAGE_WIDTH+`PLANE_WIDTH) ;
				`endif
			    `endif
		
		  	    `ifdef PAGE_PLANE_BLOCK_LUN_WIDTH_LTE_8
		     		reg_lun <= wr_data_from_nfc[`PAGE_WIDTH+`PLANE_WIDTH+`BLOCK_WIDTH];
			    `endif
		
			    rg_status_addr_state <= BLOCK_ADDR ;
			    end
		     	    
		BLOCK_ADDR: begin
		  	    `ifdef PAGE_WIDTH_GT_8
		      		//page_addr_register[`PAGE_WIDTH-1:8] <= wr_data_from_nfc[`PAGE_WIDTH-8-1:0] ;
				Bit#(`PAGE_WIDTH) localp1 = {'h0,wr_data_from_nfc} ;
				Bit#(`PAGE_WIDTH) localp2 = {'h0,buff3} ;
				page_addr_register <= (localp1<<8|localp2) ;
			    `endif
		
		  	    `ifdef PAGE_PLANE_WIDTH_GT_8
		     		`ifdef PAGE_WIDTH_GT_8
		      			reg_plane <= wr_data_from_nfc[`PAGE_WIDTH-8] ;
				`elsif PAGE_WIDTH_E_8
					reg_plane <= wr_data_from_nfc[`PAGE_WIDTH-8] ;
				`endif
			    `endif
		    
		  	    `ifdef PAGE_PLANE_BLOCK_WIDTH_GT_8
		     		`ifdef PAGE_PLANE_WIDTH_GT_8
		      			`ifdef PAGE_PLANE_BLOCK_WIDTH_LTE_16
		     				block_addr_register <= wr_data_from_nfc[`BLOCK_WIDTH+`PLANE_WIDTH+`PAGE_WIDTH-9:`PLANE_WIDTH+`PAGE_WIDTH-8];
		      			`else
		     				//block_addr_register[8-(`PAGE_WIDTH+`PLANE_WIDTH-8)-1:0] <= wr_data_from_nfc[7:`PLANE_WIDTH+`PAGE_WIDTH-8];
						buff1 <= wr_data_from_nfc >> (`PAGE_WIDTH+`PLANE_WIDTH-8) ;
					`endif
				`elsif PAGE_PLANE_WIDTH_E_8
		      			`ifdef PAGE_PLANE_BLOCK_WIDTH_LTE_16
		     				block_addr_register <= wr_data_from_nfc[`BLOCK_WIDTH+`PLANE_WIDTH+`PAGE_WIDTH-9:`PLANE_WIDTH+`PAGE_WIDTH-8];
		      			`else
		     				//block_addr_register[8-(`PAGE_WIDTH+`PLANE_WIDTH-8)-1:0] <= wr_data_from_nfc[7:`PLANE_WIDTH+`PAGE_WIDTH-8];
						buff1 <= wr_data_from_nfc >> (`PAGE_WIDTH+`PLANE_WIDTH-8) ;
					`endif
		     		`else
      		      			`ifdef PAGE_PLANE_BLOCK_WIDTH_LTE_16
		     			   //block_addr_register[`BLOCK_WIDTH-1:(8-`PLANE_WIDTH-`PAGE_WIDTH)] <= wr_data_from_nfc[`BLOCK_WIDTH-(8-`PLANE_WIDTH-`PAGE_WIDTH)-1:0];
					   Bit#(`BLOCK_WIDTH) local1 = {'h0,(wr_data_from_nfc << (16-`PLANE_WIDTH-`PAGE_WIDTH-`BLOCK_WIDTH))};
					   Bit#(`BLOCK_WIDTH) local2 = {'h0,buff1};
					   block_addr_register <= (local1<<(`BLOCK_WIDTH-8))|local2 ;
		      			`else
		     	                   //block_addr_register[8+(8-`PLANE_WIDTH-`PAGE_WIDTH-1):(8-`PLANE_WIDTH-`PAGE_WIDTH)] <= wr_data_from_nfc;
					   buff2 <= wr_data_from_nfc ;
					`endif
				`endif
			    `endif
		     	
			    `ifdef PAGE_PLANE_BLOCK_LUN_WIDTH_GT_8
			    	`ifdef PAGE_PLANE_BLOCK_LUN_WIDTH_LTE_16
		      			reg_lun <= wr_data_from_nfc[`PAGE_WIDTH+`PLANE_WIDTH+`BLOCK_WIDTH-9];
				`endif
			    `endif
		      
			      rg_status_addr_state <= LUN_ADDR ;
			    end
		LUN_ADDR  : begin
		            `ifdef PAGE_PLANE_BLOCK_WIDTH_GT_16
		      		//block_addr_register[`BLOCK_WIDTH-1:8+(8-`PLANE_WIDTH-`PAGE_WIDTH)] <= wr_data_from_nfc[`PAGE_WIDTH+`PLANE_WIDTH+`BLOCK_WIDTH-16-1:0] ;
				`ifdef PAGE_PLANE_WIDTH_LT_8
				  Bit#(`BLOCK_WIDTH) local3 = {'h0,(wr_data_from_nfc << (24-`PLANE_WIDTH-`PAGE_WIDTH-`BLOCK_WIDTH))};
				  Bit#(`BLOCK_WIDTH) local4 = {'h0,buff1};
				  Bit#(`BLOCK_WIDTH) local5 = {'h0,buff2};
				  block_addr_register <= (local3<<(`BLOCK_WIDTH-8))|(local5<<(8-`PAGE_WIDTH-`PLANE_WIDTH))|local4 ;
				`else
				  Bit#(`BLOCK_WIDTH) local6 = {'h0,buff1};
				  Bit#(`BLOCK_WIDTH) local7 = {'h0,(wr_data_from_nfc << (24-`PLANE_WIDTH-`PAGE_WIDTH-`BLOCK_WIDTH))};
				  block_addr_register <= (local7<<(`BLOCK_WIDTH-8))|local6 ;
				`endif
			    `endif
				
		  	    `ifdef PAGE_PLANE_BLOCK_LUN_WIDTH_GT_16
		    		reg_lun <= wr_data_from_nfc[`PAGE_WIDTH+`PLANE_WIDTH+`BLOCK_WIDTH-17];
			    `endif
		            
			    rg_status_addr_state <= PAGE_ADDR ;
			    rg_op_state_global   <= READ_STATUS ;
			    end
		default   : begin
			      rg_status_addr_state <= COL_ADDR1 ; //Ideally control should not arrive here
			    end
		endcase	
						end
			'h05		    :	begin
	                     	    	  	case(rg_read_addr_state)
	              	     	    	  	COL_ADDR1 : begin
	              	     	    	  		    `ifdef COLUMN_WIDTH_LT_8
	              	     	    	  			col_addr_register  <= wr_data_from_nfc[`COLUMN_WIDTH:0] ;
							    `elsif COLUMN_WIDTH_E_8 
							        col_addr_register  <= wr_data_from_nfc[`COLUMN_WIDTH:0] ;
	              	     	    	  		    `else 
	              	     	    	  			//col_addr_register[7:0] <= wr_data_from_nfc ;
								buff4 <= wr_data_from_nfc ;
							    `endif
	              	     	    	  		
	              	     	    	  		    rg_read_addr_state <= COL_ADDR2 ;
	              	     	    	  		    end
	              	     	    	  	COL_ADDR2 : begin
	              	     	    	  		    	`ifdef COLUMN_WIDTH_GT_8
								    //col_addr_register[`COLUMN_WIDTH-1:8]  <= wr_data_from_nfc[`COLUMN_WIDTH-9:0] ;
								    Bit#(TAdd#(`COLUMN_WIDTH,1)) localc1 = {'h0,buff4} ;
								    Bit#(TAdd#(`COLUMN_WIDTH,1)) localc2 = {'h0,wr_data_from_nfc} ;
	              	     	    	  			    col_addr_register <= (localc2<<8)|localc1 ;
								`endif
	              	     	    	  		    
	              	     	    	  		    	rg_read_addr_state <= COL_ADDR1 ;
							    end
	              	     	    	  	default   : begin
	              	     	    	  		      rg_read_addr_state <= COL_ADDR1 ; //Ideally control should not arrive here
	              	     	    	  		    end
	              	     	    	  	endcase
						end
			'h06,'h00,'h80,'h85 :	begin
	case(rg_read_addr_state)
		COL_ADDR1 : begin
			   `ifdef COLUMN_WIDTH_LT_8
	              	     	col_addr_register  <= wr_data_from_nfc[`COLUMN_WIDTH:0] ;
		           `elsif COLUMN_WIDTH_E_8 
				col_addr_register  <= wr_data_from_nfc[`COLUMN_WIDTH:0] ;
	              	   `else
	              	   	//col_addr_register[7:0] <= wr_data_from_nfc ;
				buff4 <= wr_data_from_nfc ;
			   `endif
			
			    rg_read_addr_state <= COL_ADDR2 ;
			    end
		COL_ADDR2 : begin
			    `ifdef COLUMN_WIDTH_GT_8
			    	//col_addr_register[`COLUMN_WIDTH-1:8]  <= wr_data_from_nfc[`COLUMN_WIDTH-8-1:0] ;
			    	Bit#(TAdd#(`COLUMN_WIDTH,1)) localc3 = {'h0,buff4} ;
			    	Bit#(TAdd#(`COLUMN_WIDTH,1)) localc4 = {'h0,wr_data_from_nfc} ;
			    	col_addr_register <= (localc4<<8)|localc3 ;
	              	    `endif
			    
			    rg_read_addr_state <= PAGE_ADDR ;
			    end
		PAGE_ADDR : begin
		            `ifdef PAGE_WIDTH_LT_8
		     		page_addr_register   <= wr_data_from_nfc[`PAGE_WIDTH-1:0] ;
		           	`ifdef PAGE_PLANE_WIDTH_LT_8
			 		reg_plane <= wr_data_from_nfc[`PAGE_WIDTH] ;
				`elsif PAGE_PLANE_WIDTH_E_8
					reg_plane <= wr_data_from_nfc[`PAGE_WIDTH] ;
				`endif
			    `elsif `define PAGE_WIDTH_E_8
				page_addr_register   <= wr_data_from_nfc[`PAGE_WIDTH-1:0] ;
		           	`ifdef PAGE_PLANE_WIDTH_LT_8
			 		reg_plane <= wr_data_from_nfc[`PAGE_WIDTH] ;
				`elsif PAGE_PLANE_WIDTH_E_8
					reg_plane <= wr_data_from_nfc[`PAGE_WIDTH] ;
				`endif
		  	    `else
		       		 buff3   <= wr_data_from_nfc ;
				 //page_addr_register[7:0]   <= wr_data_from_nfc ;
			     `endif
		
		  	    `ifdef PAGE_PLANE_BLOCK_WIDTH_LTE_8
		     		block_addr_register <= wr_data_from_nfc[`PAGE_WIDTH+`BLOCK_WIDTH+`PLANE_WIDTH-1:`PAGE_WIDTH+`PLANE_WIDTH];
			    `elsif PAGE_PLANE_BLOCK_WIDTH_GT_8
			    	`ifdef PAGE_PLANE_WIDTH_LT_8
					//block_addr_register[7-(`PAGE_WIDTH+`PLANE_WIDTH):0] <= wr_data_from_nfc[7:(`PAGE_WIDTH+`PLANE_WIDTH)];
			       		buff1 <= wr_data_from_nfc >> (`PAGE_WIDTH+`PLANE_WIDTH) ;
				`endif
			    `endif
		
		  	    `ifdef PAGE_PLANE_BLOCK_LUN_WIDTH_LTE_8
		     		reg_lun <= wr_data_from_nfc[`PAGE_WIDTH+`PLANE_WIDTH+`BLOCK_WIDTH];
			    `endif
		
			    rg_read_addr_state <= BLOCK_ADDR;
			    end
		     	    
		BLOCK_ADDR: begin
		  	    `ifdef PAGE_WIDTH_GT_8
		      		//page_addr_register[`PAGE_WIDTH-1:8] <= wr_data_from_nfc[`PAGE_WIDTH-8-1:0] ;
				Bit#(`PAGE_WIDTH) localpp1 = {'h0,wr_data_from_nfc} ;
				Bit#(`PAGE_WIDTH) localpp2 = {'h0,buff3} ;
				page_addr_register <= (localpp1<<8|localpp2) ;
			    `endif
		
		  	    `ifdef PAGE_PLANE_WIDTH_GT_8
		     		`ifdef PAGE_WIDTH_GT_8
		      			reg_plane <= wr_data_from_nfc[`PAGE_WIDTH-8] ;
				`elsif PAGE_WIDTH_E_8
					reg_plane <= wr_data_from_nfc[`PAGE_WIDTH-8] ;
				`endif
			    `endif
		    
		  	    `ifdef PAGE_PLANE_BLOCK_WIDTH_GT_8
		     		`ifdef PAGE_PLANE_WIDTH_GT_8
		      			`ifdef PAGE_PLANE_BLOCK_WIDTH_LTE_16
		     				block_addr_register <= wr_data_from_nfc[`BLOCK_WIDTH+`PLANE_WIDTH+`PAGE_WIDTH-9:`PLANE_WIDTH+`PAGE_WIDTH-8];
		      			`else
		     				//block_addr_register[8-(`PAGE_WIDTH+`PLANE_WIDTH-8)-1:0] <= wr_data_from_nfc[7:`PLANE_WIDTH+`PAGE_WIDTH-8];
						buff1 <= wr_data_from_nfc >> (`PAGE_WIDTH+`PLANE_WIDTH-8) ;
					`endif
				`elsif PAGE_PLANE_WIDTH_E_8
		      			`ifdef PAGE_PLANE_BLOCK_WIDTH_LTE_16
		     				block_addr_register <= wr_data_from_nfc[`BLOCK_WIDTH+`PLANE_WIDTH+`PAGE_WIDTH-9:`PLANE_WIDTH+`PAGE_WIDTH-8];
		      			`else
		     				//block_addr_register[8-(`PAGE_WIDTH+`PLANE_WIDTH-8)-1:0] <= wr_data_from_nfc[7:`PLANE_WIDTH+`PAGE_WIDTH-8];
						buff1 <= wr_data_from_nfc >> (`PAGE_WIDTH+`PLANE_WIDTH-8) ;
					`endif
		     		`else
      		      			`ifdef PAGE_PLANE_BLOCK_WIDTH_LTE_16
		     			   //block_addr_register[`BLOCK_WIDTH-1:(8-`PLANE_WIDTH-`PAGE_WIDTH)] <= wr_data_from_nfc[`BLOCK_WIDTH-(8-`PLANE_WIDTH-`PAGE_WIDTH)-1:0];
					   Bit#(`BLOCK_WIDTH) locall1 = {'h0,(wr_data_from_nfc << (16-`PLANE_WIDTH-`PAGE_WIDTH-`BLOCK_WIDTH))};
					   Bit#(`BLOCK_WIDTH) locall2 = {'h0,buff1};
					   block_addr_register <= (locall1<<(`BLOCK_WIDTH-8))|locall2 ;
		      			`else
		     	                   //block_addr_register[8+(8-`PLANE_WIDTH-`PAGE_WIDTH-1):(8-`PLANE_WIDTH-`PAGE_WIDTH)] <= wr_data_from_nfc;
					   buff2 <= wr_data_from_nfc ;
					`endif
				`endif
			    `endif
		     	
			    `ifdef PAGE_PLANE_BLOCK_LUN_WIDTH_GT_8
			    	`ifdef PAGE_PLANE_BLOCK_LUN_WIDTH_LTE_16
		      			reg_lun <= wr_data_from_nfc[`PAGE_WIDTH+`PLANE_WIDTH+`BLOCK_WIDTH-9];
				`endif
			    `endif
		      
			      rg_read_addr_state <= LUN_ADDR ;
			    end
		LUN_ADDR  : begin
		            `ifdef PAGE_PLANE_BLOCK_WIDTH_GT_16
		      		//block_addr_register[`BLOCK_WIDTH-1:8+(8-`PLANE_WIDTH-`PAGE_WIDTH)] <= wr_data_from_nfc[`PAGE_WIDTH+`PLANE_WIDTH+`BLOCK_WIDTH-16-1:0] ;
				`ifdef PAGE_PLANE_WIDTH_LT_8
				  Bit#(`BLOCK_WIDTH) locall3 = {'h0,(wr_data_from_nfc << (24-`PLANE_WIDTH-`PAGE_WIDTH-`BLOCK_WIDTH))};
				  Bit#(`BLOCK_WIDTH) locall4 = {'h0,buff1};
				  Bit#(`BLOCK_WIDTH) locall5 = {'h0,buff2};
				  block_addr_register <= (locall3<<(`BLOCK_WIDTH-8))|(locall5<<(8-`PAGE_WIDTH-`PLANE_WIDTH))|locall4 ;
				`else
				  Bit#(`BLOCK_WIDTH) locall6 = {'h0,buff1};
				  Bit#(`BLOCK_WIDTH) locall7 = {'h0,(wr_data_from_nfc << (24-`PLANE_WIDTH-`PAGE_WIDTH-`BLOCK_WIDTH))};
				  block_addr_register <= (locall7<<(`BLOCK_WIDTH-8))|locall6 ;
				`endif
			    `endif
				
		  	    `ifdef PAGE_PLANE_BLOCK_LUN_WIDTH_GT_16
		    		reg_lun <= wr_data_from_nfc[`PAGE_WIDTH+`PLANE_WIDTH+`BLOCK_WIDTH-17];
				Bit#(`LUN_WIDTH) lun_got = wr_data_from_nfc[`PAGE_WIDTH+`PLANE_WIDTH+`BLOCK_WIDTH-17];
			    `else
			    	Bit#(`LUN_WIDTH) lun_got = reg_lun;
			    `endif
			    
			    if(command_register == 'h80 || command_register == 'h85)
			    begin
				rg_op_state_global <= PRGM_CACHE_REG ; // From nxt clock write data into cache register.
				cache_reg_select   <= {lun_got,reg_plane} ;
				read_reg_address[{lun_got,reg_plane}]   <= col_addr_register ;
			    end
			    
			    	rg_read_addr_state <= COL_ADDR1 ;
		     	    end
		default   : begin
			      rg_read_addr_state <= COL_ADDR1 ; //Ideally control should not arrive here
			    end
		endcase
		end
	                default		    :  	begin
							rg_op_state_global <= IDLE ; //Ideally should not come here .
						end
		endcase					
	endrule
	
	//Here can write into the corresponding cache register only when the request is from the NFC and not from the target.
	//Rule will fire only when the NFC has pulled write enable low and toggles.
	rule rl_data_write_cache_reg_from_nfc (wr_onfi_ce_n == 1'b0 && wr_onfi_we_n == 1'b0 && wr_onfi_ale == 1'b0 && wr_onfi_cle == 1'b0 && wr_onfi_re_n == 1'b1 && rg_op_state_global == PRGM_CACHE_REG);
		//Write into the corresponding cache register picked from LUN and block addressing
		cache_reg[cache_reg_select].portA.request.put ( BRAMRequest{
					        	    			write : True, 
					        	    			address: read_reg_address[cache_reg_select], 
					        	    			datain : wr_data_from_nfc , 
					        	    			responseOnWrite : False} 
					      			       ) ;
		if(read_reg_address[cache_reg_select] < `VALID_COL_ADDR)
		begin
			read_reg_address[cache_reg_select]  <= read_reg_address[cache_reg_select] + 1;
		end
		else
		begin
			read_reg_address[cache_reg_select]   <= 'h0 ;
			rg_op_state_global <= IDLE ;
		end
	endrule
	
	//Rule will fire only when the NFC has pulled read enable low and the state is read cache and not any id or feature operation.
	rule rl_data_read_cache_reg_to_nfc (wr_onfi_ce_n == 1'b0 && wr_onfi_we_n == 1'b1 && wr_onfi_ale == 1'b0 && wr_onfi_cle == 1'b0 && wr_onfi_re_n == 1'b0 && rg_op_state_global == READ_CACHE_REG);
		//Read from the corresponding cache register picked from LUN and block addressing
		cache_reg[cache_reg_select].portA.request.put ( BRAMRequest{
					        	    			write : False, 
					        	    			address: read_reg_address[cache_reg_select], 
					        	    			datain : ? , 
					        	    			responseOnWrite : False} 
					      			       ) ;
		if(read_reg_address[cache_reg_select] < `VALID_COL_ADDR)
		begin
			read_reg_address[cache_reg_select] <= read_reg_address[cache_reg_select] + 1;
		end
		else
		begin
			read_reg_address[cache_reg_select]    <= 'h0 ;
			rg_op_state_global  <= IDLE ;
		end
	endrule
	
	//Below rules cannot be generic , since rle will not fire if all requests are put under one rule.
	rule rl_read_portA_cache_reg_0 ; // fired 1 cycle after 'cache_reg.portA.request.put' is called in data read cycle.
		let data <- cache_reg[0].portA.response.get() ;
                rg_data_to_nfc <= data ;
                $display ( " **** NAND ****8 bit data sent nand cache0 to nfc" ) ;
	endrule
	
	rule rl_read_portA_cache_reg_1 ; // fired 1 cycle after 'cache_reg.portA.request.put' is called in data read cycle.
		let data <- cache_reg[1].portA.response.get() ;
                rg_data_to_nfc <= data ;
                $display ( " **** NAND ****8 bit data sent nand cache1 to nfc" ) ;
	endrule
	
	rule rl_read_portA_cache_reg_2 ; // fired 1 cycle after 'cache_reg.portA.request.put' is called in data read cycle.
		let data <- cache_reg[2].portA.response.get() ;
                rg_data_to_nfc <= data ;
                $display ( " **** NAND ****8 bit data sent nand cache2 to nfc" ) ;
	endrule
	
	rule rl_read_portA_cache_reg_3 ; // fired 1 cycle after 'cache_reg.portA.request.put' is called in data read cycle.
		let data <- cache_reg[3].portA.response.get() ;
                rg_data_to_nfc <= data ;
                $display ( " **** NAND ****8 bit data sent nand cache3 to nfc" ) ;
	endrule
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////                    FROM HERE WE ARE DEFINING THE ID MEMORY, FEATURE MEMORY, UID MEMORY, PARAMETER MEMORY OPERTION RULES                                    //////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//Rules for all features start
	rule rl_read_id (rg_op_state_global == READ_ID && wr_onfi_re_n == 1'b0) ;
		case (rg_read_state) matches
		        READ : begin
					id_memory.portA.request.put ( BRAMRequest{
	                					      write : False, 
	                					      address: id_config_addr_register[3:0] , 
	                					      datain : ?, 
	                					      responseOnWrite : False} 
	                				       ) ;
					if(id_config_addr_register == 'h3 || id_config_addr_register == 'h8)
					begin
						id_config_addr_register <= 0 ;
						rg_read_state           <= WAIT;
					end
					else
					begin
						id_config_addr_register <= id_config_addr_register+1;
						rg_read_state           <= READ;
					end
				end
			WAIT  :	begin
					rg_read_state           <= READ ; //This wait is to make sure the read request for id_memory fires at this cycle.
					rg_op_state_global      <= IDLE ;
				end
		endcase
	endrule
	
	rule rl_read_param (rg_op_state_global == READ_PARAM && wr_onfi_re_n == 1'b0) ;
		param_memory.portA.request.put ( BRAMRequest{
	        			      		write : False, 
	        			      		address: id_config_addr_register[7:0] , 
	        			      		datain : ?, 
	        			      		responseOnWrite : False} 
	        		               ) ;
		if(id_config_addr_register == 'hFF)
		begin
			id_config_addr_register <= 'h0;
			rg_op_state_global      <= IDLE ;
		end
		else
		begin
			id_config_addr_register <= id_config_addr_register+1;
		end
	endrule
	
	rule rl_read_uid (rg_op_state_global == READ_UID && wr_onfi_re_n == 1'b0) ;
		uid_memory.portA.request.put ( BRAMRequest{
	        			      write : False, 
	        			      address: id_config_addr_register[4:0] , 
	        			      datain : ?, 
	        			      responseOnWrite : False} 
	        		       ) ;
		if(id_config_addr_register == 'h1F)
		begin
			id_config_addr_register <= 'h0;
			rg_op_state_global      <= IDLE ;
		end
		else
		begin
			id_config_addr_register <= id_config_addr_register+1;
		end
	endrule
	
	//This is to send/get the feature parameters which are 4B in 4 cycles.
	Reg#(Bit#(2)) send_feature_buff <- mkReg(0);
	Reg#(Bit#(24)) feature_buff <- mkReg(0);
	
	rule rl_read_feature (rg_op_state_global == READ_FEATURE && wr_onfi_re_n == 1'b0) ;
	
		feature_memory.portA.request.put ( BRAMRequest{
	        			      			write : False, 
	        			      			address: id_config_addr_register ,  
	        			      			datain : ?, 
	        			      			responseOnWrite : False} 
	        		       		 ) ;
		rg_op_state_global      <= IDLE ;
	endrule
	
	rule rl_set_feature (rg_op_state_global == SET_FEATURE && wr_onfi_we_n == 1'b0) ;
		case(rg_write_state) matches 
			WRITE : begin
					//Store as {P4,P3,P2,P1} parameter order.
					if(send_feature_buff == 'h0)
					begin
						rg_write_state <=  WRITE ;
						feature_buff[7:0] <= wr_data_from_nfc ;
					end
					else if (send_feature_buff == 'h1) 
					begin
						rg_write_state <=  WRITE ;
						feature_buff[15:8] <= wr_data_from_nfc ;
					end
					else
					begin
						feature_buff[23:16] <= wr_data_from_nfc ;
						rg_write_state <=  WRITE_1 ;
					end
					send_feature_buff <= send_feature_buff + 'h1 ;
				end
		      WRITE_1 :	begin
					feature_memory.portA.request.put ( BRAMRequest{
	        			      					write : True, 
	        			      					address: id_config_addr_register , 
	        			      					datain : {wr_data_from_nfc,feature_buff[23:0]}, 
	        			      					responseOnWrite : False} 
	        		       					) ;
					rg_write_state      <=  WRITE ;
					rg_op_state_global  <= IDLE ;
					send_feature_buff   <= 'h0 ;
				end
		      default : begin
		      			//Ideally control should not arrive here
					rg_write_state      <=  WRITE ;
					rg_op_state_global <= IDLE ;
				end
		endcase
	endrule
	
	//This rule is specific to the currrent implementation of 2 LUNs in the target and not generic.
	rule rl_read_status (rg_op_state_global == READ_STATUS && wr_onfi_re_n == 1'b0) ;
		if(reg_lun == 1'b0)
		begin
			rg_data_to_nfc     <= {'h0,wr_onfi_wp_n,flag_RDY[0]&flag_RDY[1],flag_ARDY[0]&flag_ARDY[1],3'b0,flag_FAILC[0]|flag_FAILC[1],flag_FAIL[0]|flag_FAIL[1]};
		end
		else
		begin
			rg_data_to_nfc <= {'h0,wr_onfi_wp_n,flag_RDY[2]&flag_RDY[3],flag_ARDY[2]&flag_ARDY[3],3'b0,flag_FAILC[2]|flag_FAILC[3],flag_FAIL[2]|flag_FAIL[3]};
		end
	endrule
	
	
	rule rl_read_portA_id_memory ; // fired 1 cycle after 'id_memory.portA.request.put' is called in data read cycle.
        	let data <- id_memory.portA.response.get( ) ;
                rg_data_to_nfc <= data ;
                $display ( " **** NAND ****8 bit id sent nand cache to nfc" ) ;
	endrule
	
	rule rl_read_portA_param_memory ; // fired 1 cycle after 'param_memory.portA.request.put' is called in data read cycle.
        	let data <- param_memory.portA.response.get( ) ;
                rg_data_to_nfc <= data ;
                $display ( " **** NAND ****8 bit parameter sent nand cache to nfc" ) ;
	endrule
	
	rule rl_read_portA_uid_memory ; // fired 1 cycle after 'uid_memory.portA.request.put' is called in data read cycle.
        	let data <- uid_memory.portA.response.get( ) ;
                rg_data_to_nfc <= data ;
                $display ( " **** NAND ****8 bit uid sent nand cache to nfc" ) ;
	endrule
	
	
	rule rl_read_portA_feature_memory ; // fired 1 cycle after 'feature_memory.portA.request.put' is called in data read cycle.
        	let data <- feature_memory.portA.response.get( ) ;
                //Read as {P4,P3,P2,P1} parameter order.
		rg_data_to_nfc <= data[7:0] ;
		feature_buff   <= data[31:8];
		send_feature_buff <= 'h1 ;
                $display ( " **** NAND ****8 bit feature sent nand cache to nfc" ) ;
	endrule
	
	rule rl_read_feature_memory_cont(send_feature_buff > 0 && wr_onfi_re_n == 1'b0);
		rg_data_to_nfc <= feature_buff[7:0] ;
		feature_buff   <= feature_buff >> 8 ;
		$display ( " **** NAND ****8 bit feature sent nand cache to nfc" ) ;
		if(send_feature_buff == 'h3)
			send_feature_buff <= 'h0 ;
		else
			send_feature_buff <= send_feature_buff + 'h1 ;
	endrule

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                         BUSY FLAG UPDATION                                                                              ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Below rule is to update the busy flag during read from every plane. Not firing this rl for data register operation,(only cache reg operation).
	rule rl_read_busy_update ;
		rg_t_ready_busy_n <= flag_RDY[0] & flag_RDY[1] & flag_RDY[2] & flag_RDY[3] ;
	endrule	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                         BUSY FLAG UPDATION DONE                                                                         ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                    OPERATION STATE RESET (not related to nand model)                                                    ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Either both planes in lun will be in read state or one of them will be. The operation will finish @ same cycle for both planes in case of multi plane read. Hence the final state of the flag_read_queued_plane[1:0] will be zero if no more read operation is pending in lun0. Similarly for the program and erase cases over all lun's.

	rule rl_lun0_read_op_state_reset ((rg_op_state[0] == READ||rg_op_state[0] == READ_PAGE_CACHE)&&(flag_read_queued_plane[0] == 0 && flag_read_queued_plane[1] == 0 )); 
		rg_op_state[0] <= IDLE ;
	endrule
	
	rule rl_lun1_read_op_state_reset ((rg_op_state[1] == READ||rg_op_state[1] == READ_PAGE_CACHE)&&(flag_read_queued_plane[2] == 0 && flag_read_queued_plane[3] == 0 )); 
		rg_op_state[1] <= IDLE ;
	endrule
	
	rule rl_lun0_read_cache_op_state_reset (rg_op_state[0] == READ_PAGE_CACHE_LAST &&(flag_read_queued_plane_last[0] == 1 || flag_read_queued_plane_last[1] == 1 )); 
		if(wait_read_page_cache[0] == 0) //This means actual read page cache last (3F).
			rg_op_state[0] <= IDLE ;
		else
		begin
			rg_op_state[0] <= READ_PAGE_CACHE ; // Start reading from target page to data register in next cycle.
			if(flag_read_queued_plane_last[0] == 1)
				flag_read_queued_plane[0] <= 1;
			else
				flag_read_queued_plane[0] <= 0;
			if(flag_read_queued_plane_last[1] == 1)
				flag_read_queued_plane[1] <= 1;
			else
				flag_read_queued_plane[1] <= 0;
			flag_read_queued_plane_last[0] <= 0;
			flag_read_queued_plane_last[1] <= 0;
		end
	endrule
	
	rule rl_lun0_prgm_op_state_reset ((rg_op_state[0] == PROGRAM ) && (flag_prgm_queued_plane[0] == 0 && flag_prgm_queued_plane[1] == 0)); 
		rg_op_state[0] <= IDLE ;
	endrule
	
	rule rl_lun1_prgm_op_state_reset ((rg_op_state[1] == PROGRAM ) && (flag_prgm_queued_plane[2] == 0 && flag_prgm_queued_plane[3] == 0)); 
		rg_op_state[1] <= IDLE ;
	endrule
	
	rule rl_lun0_prgm_last_op_state_reset ((rg_op_state[0] == PROGRAM_PAGE_CACHE_LAST) && (flag_prgm_queued_plane_last[0] == 0 && flag_prgm_queued_plane_last[1] == 0)); 
		if(wait_program_page_cache[0] == 1)
		begin
			rg_op_state[0]             <= PROGRAM_PAGE_CACHE ;
			wait_program_page_cache[0] <= 0;
		end
		else
			rg_op_state[0] <= IDLE ;
	endrule
	
	rule rl_lun1_prgm_last_op_state_reset ((rg_op_state[1] == PROGRAM_PAGE_CACHE_LAST) && (flag_prgm_queued_plane_last[2] == 0 && flag_prgm_queued_plane_last[3] == 0)); 
		if(wait_program_page_cache[1] == 1)
		begin
			rg_op_state[1] <= PROGRAM_PAGE_CACHE ;
			wait_program_page_cache[1] <= 0;
		end
		else
			rg_op_state[1] <= IDLE ;
	endrule
	
//We need to backup the copies of queued_block,queued_page and flag_prgm_queued_plane in queued_block_last,queued_page_last and flag_prgm_queued_plane_last before starting program page cache, since while program page cache last is happening there is possibility of getting next program cache command.
	rule rl_lun0_prgm_cache_op_state_reset ((rg_op_state[0] == PROGRAM_PAGE_CACHE) && (flag_prgm_last[0] == 1 || flag_prgm_last[1] == 1)); 
		rg_op_state[0]    <= PROGRAM_PAGE_CACHE_LAST ;
		if(flag_prgm_queued_plane[0] == 1)
		begin
			flag_prgm_queued_plane_last[0] <= 1;
			queued_block_last[0]           <= queued_block[0] ;
			queued_page_last[0]            <= queued_page[0] ;
			flag_RDY[0]                    <= 1; //Update flag_RDY signal. since from here on only ARDY flag will be 0 indicating array operation ON.
		end
		else
			flag_prgm_queued_plane_last[0] <= 0;
			
		if(flag_prgm_queued_plane[1] == 1)
		begin
			flag_prgm_queued_plane_last[1] <= 1;
			queued_block_last[1]           <= queued_block[1] ;
			queued_page_last[1]            <= queued_page[1] ;
			flag_RDY[1]                    <= 1; //Update flag_RDY signal. since from here on only ARDY flag will be 0 indicating array operation ON.
		end
		else
			flag_prgm_queued_plane_last[1] <= 0;
		flag_prgm_queued_plane[0] <= 0;
		flag_prgm_queued_plane[1] <= 0;
		flag_prgm_last[0] <= 0;
		flag_prgm_last[1] <= 0;
	endrule
	
	rule rl_lun1_prgm_cache_op_state_reset ((rg_op_state[1] == PROGRAM_PAGE_CACHE) && (flag_prgm_last[2] == 1 || flag_prgm_last[3] == 1)); 
		rg_op_state[1]    <= PROGRAM_PAGE_CACHE_LAST ;
		if(flag_prgm_queued_plane[2] == 1)
		begin
			flag_prgm_queued_plane_last[2] <= 1;
			queued_block_last[2]           <= queued_block[2] ;
			queued_page_last[2]            <= queued_page[2] ;
			flag_RDY[2]                    <= 1; //Update flag_RDY signal. since from here on only ARDY flag will be 0 indicating array operation ON.
		end
		else
			flag_prgm_queued_plane_last[2] <= 0;
			
		if(flag_prgm_queued_plane[3] == 1)
		begin
			flag_prgm_queued_plane_last[3] <= 1;
			queued_block_last[3]           <= queued_block[3] ;
			queued_page_last[3]            <= queued_page[3] ;
			flag_RDY[3]                    <= 1; //Update flag_RDY signal. since from here on only ARDY flag will be 0 indicating array operation ON.
		end
		else
			flag_prgm_queued_plane_last[3] <= 0;
		flag_prgm_queued_plane[2] <= 0;
		flag_prgm_queued_plane[3] <= 0;
		flag_prgm_last[2] <= 0;
		flag_prgm_last[3] <= 0;
	endrule
	
	rule rl_lun0_erase_op_state_reset (rg_op_state[0] == ERASE && flag_erase_queued_plane[0] == 0 && flag_erase_queued_plane[1] == 0); 
		rg_op_state[0] <= IDLE ;
	endrule
	
	rule rl_lun1_erase_op_state_reset (rg_op_state[1] == ERASE && flag_erase_queued_plane[2] == 0 && flag_erase_queued_plane[2] == 0); 
		rg_op_state[1] <= IDLE ;
	endrule
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                              END OF OPERATION STATE RESET                                                               ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////                      FROM HERE WE ARE MODLLING THE FLASH MEMORY. REPLACE THIS MODEL WITH THE ACTUAL NAND FLASH MODEL                                //////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // From here on the implementation is specific to the curent target architecture of 2 planes/LUN and 2 LUNs/target and not generic. This is crude modelling of target.

	//ready_Busy pin controlled by the target . Hence rg_t_ready_busy_n can be raised or lowered only here. 
	//Creating Target from here. We will emulate 2 blocks per plane , with each plane containing 2 pages. total of 2 luns and 2 planes.
	//each block will be 4096+128 bytes of data. (i.e 12 address lines , each column of 2 bytes data).
	//We will emulate the following cases as below
	// LUN0 , Plane0 , Block0, Page0
	// LUN0 , Plane0 , Block0, Page1
	// LUN0 , Plane0 , Block1, Page0
	// LUN0 , Plane0 , Block1, Page1
	// LUN0 , Plane1 , Block0, Page0
	// LUN0 , Plane1 , Block0, Page1
	// LUN0 , Plane1 , Block1, Page0
	// LUN0 , Plane1 , Block1, Page1
	// LUN1 , Plane0 , Block0, Page0
	// LUN1 , Plane0 , Block0, Page1
 	// LUN1 , Plane0 , Block1, Page0
 	// LUN1 , Plane0 , Block1, Page1
 	// LUN1 , Plane1 , Block0, Page0
 	// LUN1 , Plane1 , Block0, Page1
 	// LUN1 , Plane1 , Block1, Page0
 	// LUN1 , Plane1 , Block1, Page1
	
	//Creating target array , total of 16.A total of 4 blocks in each plane created separately , since bluespec doesn't allow accessing 2 blocks if defined as vector of 16,
	//The mapping of address will be as : {Lun_address,block_address[1],plane_address,page_addr[0]} => 4 address i.e 16 cases.
	
	Vector#(TMul#(TExp#(`PAGE_WIDTH),TExp#(`BLOCK_WIDTH)), BRAM_Configure) cfg_nand_plane0; 
	//Vector#(4, BRAM_Configure) cfg_nand_plane0; 
	//for(Integer i = 0; i < 4 ; i = i + 1) begin
	for(Integer i = 0; i < ((2**`PAGE_WIDTH)*(2**`BLOCK_WIDTH)) ; i = i + 1) begin
      		cfg_nand_plane0[i] = defaultValue; 
		//Adding below line just to test the BBM request.Should be commented otherwise 
		//cfg_nand_plane0[i].loadFormat = tagged Hex "nand_initial.txt" ;
   	end
	Vector#(TMul#(TExp#(`PAGE_WIDTH),TExp#(`BLOCK_WIDTH)), BRAM_Configure) cfg_nand_plane1; 
	//Vector#(4, BRAM_Configure) cfg_nand_plane1; 
	//for(Integer i = 0; i < 4 ; i = i + 1) begin 
	for(Integer i = 0; i < ((2**`PAGE_WIDTH)*(2**`BLOCK_WIDTH)) ; i = i + 1) begin
      		cfg_nand_plane1[i] = defaultValue; 
   	end
	Vector#(TMul#(TExp#(`PAGE_WIDTH),TExp#(`BLOCK_WIDTH)), BRAM_Configure) cfg_nand_plane2; 
	//Vector#(4, BRAM_Configure) cfg_nand_plane2; 
	//for(Integer i = 0; i < 4 ; i = i + 1) begin 
	for(Integer i = 0; i < ((2**`PAGE_WIDTH)*(2**`BLOCK_WIDTH)) ; i = i + 1) begin
      		cfg_nand_plane2[i] = defaultValue; 
   	end
 	Vector#(TMul#(TExp#(`PAGE_WIDTH),TExp#(`BLOCK_WIDTH)), BRAM_Configure) cfg_nand_plane3; 
	//Vector#(4, BRAM_Configure) cfg_nand_plane3; 
	//for(Integer i = 0; i < 4 ; i = i + 1) begin 
	for(Integer i = 0; i < ((2**`PAGE_WIDTH)*(2**`BLOCK_WIDTH)) ; i = i + 1) begin
      		cfg_nand_plane3[i] = defaultValue; 
   	end
	//Using 4 pages in each plane of each LUN. 2 pages per block hence 2 blocks per lun. (Hence vector of size 4,else use vector of total_blocks_in_plane*pages_per_block
	/*Vector#(4, BRAM2Port#(Bit#(TAdd#(`COLUMN_WIDTH,1)), Bit#(8))) nand_target_plane0 <- mapM(mkBRAM2Server, cfg_nand_plane0);
	Vector#(4, BRAM2Port#(Bit#(TAdd#(`COLUMN_WIDTH,1)), Bit#(8))) nand_target_plane1 <- mapM(mkBRAM2Server, cfg_nand_plane1);
	Vector#(4, BRAM2Port#(Bit#(TAdd#(`COLUMN_WIDTH,1)), Bit#(8))) nand_target_plane2 <- mapM(mkBRAM2Server, cfg_nand_plane2);
	Vector#(4, BRAM2Port#(Bit#(TAdd#(`COLUMN_WIDTH,1)), Bit#(8))) nand_target_plane3 <- mapM(mkBRAM2Server, cfg_nand_plane3);*/
	
	Vector#(TMul#(TExp#(`PAGE_WIDTH),TExp#(`BLOCK_WIDTH)), BRAM2Port#(Bit#(TAdd#(`COLUMN_WIDTH,1)), Bit#(8))) nand_target_plane0 <- mapM(mkBRAM2Server, cfg_nand_plane0);
	Vector#(TMul#(TExp#(`PAGE_WIDTH),TExp#(`BLOCK_WIDTH)), BRAM2Port#(Bit#(TAdd#(`COLUMN_WIDTH,1)), Bit#(8))) nand_target_plane1 <- mapM(mkBRAM2Server, cfg_nand_plane1);
	Vector#(TMul#(TExp#(`PAGE_WIDTH),TExp#(`BLOCK_WIDTH)), BRAM2Port#(Bit#(TAdd#(`COLUMN_WIDTH,1)), Bit#(8))) nand_target_plane2 <- mapM(mkBRAM2Server, cfg_nand_plane2);
	Vector#(TMul#(TExp#(`PAGE_WIDTH),TExp#(`BLOCK_WIDTH)), BRAM2Port#(Bit#(TAdd#(`COLUMN_WIDTH,1)), Bit#(8))) nand_target_plane3 <- mapM(mkBRAM2Server, cfg_nand_plane3);
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                             RESET OPERATION                                                                             ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//For each LUN each plane need to go to individual block and we need to reset pages on by one,hence need a reg to point and increment to each page and block for every plane of every LUN.
	Reg#(Bit#(`PAGE_WIDTH)) page_max <- mkReg(0);
	Reg#(Bit#(`BLOCK_WIDTH)) block_max <- mkReg(0);
	
	Vector#(`TOTAL_PLANE,Reg#(Bit#(TAdd#(`COLUMN_WIDTH,1)))) reset_col_addr <- replicateM(mkReg(0)) ;
	Vector#(`TOTAL_PLANE,Reg#(Bit#(`PAGE_WIDTH)))          reset_page_addr <- replicateM(mkReg(0)) ;
	Vector#(`TOTAL_PLANE,Reg#(Bit#(`BLOCK_WIDTH)))        reset_block_addr <- replicateM(mkReg(0)) ;
	Vector#(`TOTAL_PLANE,Reg#(Bit#(1))) flag_reset_queued_plane <- replicateM(mkReg(0)) ;
	//Rule to reset all the pages (2 pages per block, 2 blocks per plane, 2 planes per LUN, and 2 LUNs) one after the other (Just crude modelling, since this will be 		        //replaced with actual NAND flash model)
	
	//Reset Lun0 plane0
	rule rl_reset_plane0 (rg_op_state_global == RESET && flag_reset_queued_plane[0] == 1'b0) ;
		nand_target_plane0[{reset_block_addr[0],reset_page_addr[0]}].portA.request.put ( BRAMRequest{
		//nand_target_plane0[{reset_block_addr[0][0],reset_page_addr[0][0]}].portA.request.put ( BRAMRequest{
					        	    						write : True, 
					        	    						address: reset_col_addr[0], 
					        	    						datain : 'hFF , 
					        	    						responseOnWrite : False} 
					      			       		       	       ) ;
		if(reset_col_addr[0] < `VALID_COL_ADDR)
		begin
			reset_col_addr[0] <= reset_col_addr[0]+1;
			flag_RDY[0]       <= 1'b0 ;
			flag_ARDY[0]      <= 1'b0 ;
		end
		else
		begin
			reset_col_addr[0]      <= 'h0;
			//if(reset_page_addr[0] == 'h1)
			if(reset_page_addr[0] == ~page_max)
			begin
				reset_page_addr[0]         <= 'h0 ;
				if(reset_block_addr[0] == 'h1)
				//if(reset_block_addr[0] == ~block_max)
				begin
					flag_reset_queued_plane[0] <= 1'b1 ;
					flag_RDY[0]                <= 1'b1 ; // Lower the ready flag since page will end here.
					flag_ARDY[0]               <= 1'b1 ;
				end
				else
					reset_block_addr[0] <= reset_block_addr[0] + 'h1 ;
			end
			else
				reset_page_addr[0] <= reset_page_addr[0] + 'h1 ;
		end
	endrule
	
	//Reset Lun0 plane1
	rule rl_reset_plane1 (rg_op_state_global == RESET && flag_reset_queued_plane[1] == 1'b0) ;
		nand_target_plane1[{reset_block_addr[1],reset_page_addr[1]}].portA.request.put ( BRAMRequest{
		//nand_target_plane1[{reset_block_addr[1][0],reset_page_addr[1][0]}].portA.request.put ( BRAMRequest{
					        	    						write : True, 
					        	    						address: reset_col_addr[1], 
					        	    						datain : 'hFF , 
					        	    						responseOnWrite : False} 
					      			       		       	       ) ;
		if(reset_col_addr[1] < `VALID_COL_ADDR)
		begin
			reset_col_addr[1] <= reset_col_addr[1]+1;
			flag_RDY[1]       <= 1'b0 ;
			flag_ARDY[1]      <= 1'b0 ;
		end
		else
		begin
			reset_col_addr[1]      <= 'h0;
			//if(reset_page_addr[1] == 'h1)
			if(reset_page_addr[1] == ~page_max)
			begin
				reset_page_addr[1]         <= 'h0 ;
				//if(reset_block_addr[1] == 'h1)
				if(reset_block_addr[1] == ~block_max)
				begin
					flag_reset_queued_plane[1] <= 1'b1 ;
					flag_RDY[1]                <= 1'b1 ; // Lower the ready flag since page will end here.
					flag_ARDY[1]               <= 1'b1 ;
				end
				else
					reset_block_addr[1] <= reset_block_addr[1] + 'h1 ;
			end
			else
				reset_page_addr[1] <= reset_page_addr[1] + 'h1 ;
		end
	endrule
	
	//Reset Lun1 plane0
	rule rl_reset_plane2 (rg_op_state_global == RESET && flag_reset_queued_plane[2] == 1'b0) ;
		nand_target_plane2[{reset_block_addr[2],reset_page_addr[2]}].portA.request.put ( BRAMRequest{
		//nand_target_plane2[{reset_block_addr[2][0],reset_page_addr[2][0]}].portA.request.put ( BRAMRequest{
					        	    						write : True, 
					        	    						address: reset_col_addr[2], 
					        	    						datain : 'hFF , 
					        	    						responseOnWrite : False} 
					      			       		       	       ) ;
		if(reset_col_addr[2] < `VALID_COL_ADDR)
		begin
			reset_col_addr[2] <= reset_col_addr[1]+1;
			flag_RDY[2]       <= 1'b0 ;
			flag_ARDY[2]      <= 1'b0 ;
		end
		else
		begin
			reset_col_addr[2]      <= 'h0;
			//if(reset_page_addr[2] == 'h1)
			if(reset_page_addr[2] == ~page_max)
			begin
				reset_page_addr[2]         <= 'h0 ;
				//if(reset_block_addr[2] == 'h1)
				if(reset_block_addr[2] == ~block_max)
				begin
					flag_reset_queued_plane[2] <= 1'b1 ;
					flag_RDY[2]                <= 1'b1 ; // Lower the ready flag since page will end here.
					flag_ARDY[2]               <= 1'b1 ;
				end
				else
					reset_block_addr[2] <= reset_block_addr[2] + 'h1 ;
			end
			else
				reset_page_addr[2] <= reset_page_addr[2] + 'h1 ;
		end
	endrule
	
	//Reset Lun1 plane1
	rule rl_reset_plane3 (rg_op_state_global == RESET && flag_reset_queued_plane[3] == 1'b0) ;
		nand_target_plane3[{reset_block_addr[3],reset_page_addr[3]}].portA.request.put ( BRAMRequest{
		//nand_target_plane3[{reset_block_addr[3][0],reset_page_addr[3][0]}].portA.request.put ( BRAMRequest{
					        	    						write : True, 
					        	    						address: reset_col_addr[3], 
					        	    						datain : 'hFF , 
					        	    						responseOnWrite : False} 
					      			       		       	       ) ;
		if(reset_col_addr[3] < `VALID_COL_ADDR)
		begin
			reset_col_addr[3] <= reset_col_addr[1]+1;
			flag_RDY[3]       <= 1'b0 ;
			flag_ARDY[3]      <= 1'b0 ;
		end
		else
		begin
			reset_col_addr[3]      <= 'h0;
			//if(reset_page_addr[3] == 'h1)
			if(reset_page_addr[3] == ~page_max)
			begin
				reset_page_addr[3]         <= 'h0 ;
				//if(reset_block_addr[3] == 'h1)
				if(reset_block_addr[3] == ~block_max)
				begin
					flag_reset_queued_plane[3] <= 1'b1 ;
					flag_RDY[3]                <= 1'b1 ; // Lower the ready flag since page will end here.
					flag_ARDY[3]               <= 1'b1 ;
				end
				else
					reset_block_addr[3] <= reset_block_addr[3] + 'h1 ;
			end
			else
				reset_page_addr[3] <= reset_page_addr[3] + 'h1 ;
		end
	endrule
	
	rule rl_reset_global_flag(rg_op_state_global == RESET && flag_reset_queued_plane[3] == 1'b1 && flag_reset_queued_plane[2] == 1'b1 && flag_reset_queued_plane[1] == 1'b1 && flag_reset_queued_plane[0] == 1'b1);
		rg_op_state_global         <= IDLE  ;
		flag_reset_queued_plane[0] <= 1'b0 ;
		flag_reset_queued_plane[1] <= 1'b0 ;
		flag_reset_queued_plane[2] <= 1'b0 ;
		flag_reset_queued_plane[3] <= 1'b0 ;
	endrule
	
	//RESET rule ends here
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                           RESET OPERATION DONE                                                                          ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                           READ FROM TARGET                                                                              ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//Rule to READ the pages (1 pages per plane). Rules are split for each plane, since each plane has to read parallely (Just crude modelling, since this will be replaced           //with actual NAND flash model)
	
	//Below for queued block we are losing the 0 index, since 0 index chooses the plane and plane is already differentiated by means of diff rules for diff planes. 
	
	//Lun0 plane 0
	rule rl_read_target_plane0 ((rg_op_state[0] == READ || rg_op_state[0] == READ_PAGE_CACHE) && flag_read_queued_plane[0] == 1'b1) ;
//Since we have nly 2 blocks per plane, only one index is used to select block address(i.e queued_block[plane][1]).On putting all blocks per plane,uncomment the following line and comment out the latter line.
		nand_target_plane0[{queued_block[0],queued_page[0]}].portA.request.put ( BRAMRequest{
		//nand_target_plane0[{queued_block[0][1],queued_page[0][0]}].portA.request.put ( BRAMRequest{
					        	    						write : False, 
					        	    						address: target_col_address[0], 
					        	    						datain : ? , 
					        	    						responseOnWrite : False} 
					      			       			    ) ;	
		if(target_col_address[0] < `VALID_COL_ADDR)
		begin
			target_col_address[0] <= target_col_address[0]+1;
			//Raise ready flag if not raised yet.
			if(rg_op_state[0] == READ)
			begin
				flag_RDY[0] <= 1'b0 ;
				flag_ARDY[0] <= 1'b0 ;
			end
			else
				flag_ARDY[0] <= 1'b0 ;
		end
		else
		begin
			target_col_address[0]     <= 'h0;
			flag_read_queued_plane[0] <= 1'b0;
			flag_RDY[0]               <= 1'b1 ; // Lower the ready flag since page will end here.
			flag_ARDY[0]              <= 1'b1 ;
		end
	endrule
	//Lun0 plane 1
	rule rl_read_target_plane1 ((rg_op_state[0] == READ || rg_op_state[0] == READ_PAGE_CACHE) && flag_read_queued_plane[1] == 1'b1) ;
		nand_target_plane1[{queued_block[1],queued_page[1]}].portA.request.put ( BRAMRequest{
		//nand_target_plane1[{queued_block[1][1],queued_page[1][0]}].portA.request.put ( BRAMRequest{
					        	    						write : False, 
					        	    						address: target_col_address[1], 
					        	    						datain : ? , 
					        	    						responseOnWrite : False} 
					      			       			    ) ;	
		if(target_col_address[1] < `VALID_COL_ADDR)
		begin
			target_col_address[1] <= target_col_address[1]+1;
			//Raise ready flag if not raised yet.
			if(rg_op_state[0] == READ)
			begin
				flag_RDY[1] <= 1'b0 ;
				flag_ARDY[1] <= 1'b0 ;
			end
			else
				flag_ARDY[1] <= 1'b0 ;	
		end
		else
		begin
			target_col_address[1]     <= 'h0;
			flag_read_queued_plane[1] <= 1'b0;
			flag_RDY[1]               <= 1'b1 ; // Lower the ready flag since page will end here.
			flag_ARDY[1]              <= 1'b1 ;
		end
	endrule
	//Lun1 plane 0
	rule rl_read_target_plane2 ((rg_op_state[1] == READ || rg_op_state[1] == READ_PAGE_CACHE) && flag_read_queued_plane[2] == 1'b1) ;
		nand_target_plane2[{queued_block[2],queued_page[2]}].portA.request.put ( BRAMRequest{
		//nand_target_plane2[{queued_block[2][1],queued_page[2][0]}].portA.request.put ( BRAMRequest{
					        	    						write : False, 
					        	    						address: target_col_address[2], 
					        	    						datain : ? , 
					        	    						responseOnWrite : False} 
					      			       			    ) ;	
		if(target_col_address[2] < `VALID_COL_ADDR)
		begin
			target_col_address[2] <= target_col_address[2]+1;
			//Raise ready flag if not raised yet.
			if(rg_op_state[1] == READ)
			begin
				flag_RDY[2] <= 1'b0 ;
				flag_ARDY[2] <= 1'b0 ;
			end
			else
				flag_ARDY[2] <= 1'b0 ;
		end
		else
		begin
			target_col_address[2]     <= 'h0;
			flag_read_queued_plane[2] <= 1'b0;
			flag_RDY[2]               <= 1'b1 ; // Lower the ready flag since page will end here.
			flag_ARDY[2]              <= 1'b1 ;
		end
	endrule
	//Lun1 plane 1
	rule rl_read_target_plane3 ((rg_op_state[1] == READ || rg_op_state[1] == READ_PAGE_CACHE) && flag_read_queued_plane[3] == 1'b1) ;
		nand_target_plane3[{queued_block[3],queued_page[3]}].portA.request.put ( BRAMRequest{
		//nand_target_plane3[{queued_block[3][1],queued_page[3][0]}].portA.request.put ( BRAMRequest{
					        	    						write : False, 
					        	    						address: target_col_address[3], 
					        	    						datain : ? , 
					        	    						responseOnWrite : False} 
					      			       			    ) ;	
		if(target_col_address[3] < `VALID_COL_ADDR)
		begin
			target_col_address[3] <= target_col_address[3]+1;
			//Raise ready flag if not raised yet.
			if(rg_op_state[1] == READ)
			begin
				flag_RDY[3] <= 1'b0 ;
				flag_ARDY[3] <= 1'b0 ;
			end
			else
				flag_ARDY[3] <= 1'b0 ;
		end
		else
		begin
			target_col_address[3]     <= 'h0;
			flag_read_queued_plane[3] <= 1'b0;
			flag_RDY[3]               <= 1'b1 ; // Lower the ready flag since page will end here.
			flag_ARDY[3]              <= 1'b1 ;
		end
	endrule
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                         END OF READ FROM TARGET                                                                         ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                      WRITE INTO CACHE REGISTER,DATA READ FROM TARGET                                                                    ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Since we have nly 2 blocks per plane,and 2 pages per block per plane, we have total of 4 pages per plane. uncomment the following line and comment out the latter line upon adding all pages in the vector f pages.
	//Here writing data being read from target into corresponding cache registers
	
	//Lun0 plane0
	for(Integer i=0 ; i < ((2**`PAGE_WIDTH)*(2**`BLOCK_WIDTH)) ; i = i + 1) 
	//for(Integer i=0 ; i < 4 ; i = i + 1) 
	begin
	rule rl_read_nand_memory_plane0_write_cache_reg(rg_op_state[0] == READ) ; // fired 1 cycle after rule "rl_read_target_plane0" is called.
        	let data <- nand_target_plane0[i].portA.response.get() ;
                cache_reg[0].portB.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: cache_reg_address[0], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					      	) ;
		if(cache_reg_address[0] < `VALID_COL_ADDR)
			cache_reg_address[0] <= cache_reg_address[0] + 1;
		else
			cache_reg_address[0] <= 'h0 ;
	endrule
	end
	
	//Lun0 Plane1
	for(Integer i=0 ; i < ((2**`PAGE_WIDTH)*(2**`BLOCK_WIDTH)) ; i = i + 1) 
	//for(Integer i=0 ; i < 4 ; i = i + 1) 
	begin
	rule rl_read_nand_memory_plane1_write_cache_reg(rg_op_state[0] == READ) ; // fired 1 cycle after rule "rl_read_target_plane1" is called.
        	let data <- nand_target_plane1[i].portA.response.get( ) ;
                cache_reg[1].portB.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: cache_reg_address[1], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					      	) ;
		if(cache_reg_address[1] < `VALID_COL_ADDR)
			cache_reg_address[1] <= cache_reg_address[1] + 1;
		else
			cache_reg_address[1] <= 'h0 ;
	endrule
	end
	
	//Lun1 Plane0
	for(Integer i=0 ; i < ((2**`PAGE_WIDTH)*(2**`BLOCK_WIDTH)) ; i = i + 1) 
	//for(Integer i=0 ; i < 4 ; i = i + 1) 
	begin
	rule rl_read_nand_memory_plane2_write_cache_reg(rg_op_state[1] == READ) ; // fired 1 cycle after rule "rl_read_target_plane2" is called.
        	let data <- nand_target_plane2[i].portA.response.get( ) ;
                cache_reg[2].portB.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: cache_reg_address[2], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					      	) ;
		if(cache_reg_address[2] < `VALID_COL_ADDR)
			cache_reg_address[2] <= cache_reg_address[2] + 1;
		else
			cache_reg_address[2] <= 'h0 ;
	endrule
	end
	
	//Lun1 Plane1
	for(Integer i=0 ; i < ((2**`PAGE_WIDTH)*(2**`BLOCK_WIDTH)) ; i = i + 1) 
	//for(Integer i=0 ; i < 4 ; i = i + 1) 
	begin
	rule rl_read_nand_memory_plane3_write_cache_reg(rg_op_state[1] == READ) ; // fired 1 cycle after rule "rl_read_target_plane3" is called.
        	let data <- nand_target_plane3[i].portA.response.get( ) ;
                cache_reg[3].portB.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: cache_reg_address[3], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					      	) ;
		if(cache_reg_address[3] < `VALID_COL_ADDR)
			cache_reg_address[3] <= cache_reg_address[3] + 1;
		else
			cache_reg_address[3] <= 'h0 ;
	endrule
	end
	
	//Done writing into all cache registers through port B
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                  END OF WRITE INTO CACHE REGISTER DATA READ FROM TARGET                                                                 ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                          WRITE INTO DATA REGISTER,DATA READ FROM TARGET                                                                 ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Here rules to write from the nand to data register. i.e sequential read and random READ_PAGE_CACHE operations.
	//Lun0 Plane0
	for(Integer i=0 ; i < ((2**`PAGE_WIDTH)*(2**`BLOCK_WIDTH)) ; i = i + 1) 
	//for(Integer i=0 ; i < 4 ; i = i + 1) 
	begin
	rule rl_from_nand_write_data_register_plane0 (rg_op_state[0] == READ_PAGE_CACHE);
		let data <- nand_target_plane0[i].portA.response.get( ) ;
		data_reg[0].portA.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: data_reg_address[0], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[0] < `VALID_COL_ADDR)
			data_reg_address[0] <= data_reg_address[0] + 1;
		else
			data_reg_address[0]     <= 'h0 ;
	endrule
	end
	
	//Lun0 Plane1
	for(Integer i=0 ; i < ((2**`PAGE_WIDTH)*(2**`BLOCK_WIDTH)) ; i = i + 1) 
	//for(Integer i=0 ; i < 4 ; i = i + 1) 
	begin
	rule rl_from_nand_write_data_register_plane1 (rg_op_state[0] == READ_PAGE_CACHE);
		let data <- nand_target_plane1[i].portA.response.get( ) ;
		data_reg[1].portA.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: data_reg_address[1], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[1] < `VALID_COL_ADDR)
			data_reg_address[1] <= data_reg_address[1] + 1;
		else
			data_reg_address[1]     <= 'h0 ;
	endrule
	end
	
	//Lun1 Plane0
	for(Integer i=0 ; i < ((2**`PAGE_WIDTH)*(2**`BLOCK_WIDTH)) ; i = i + 1) 
	//for(Integer i=0 ; i < 4 ; i = i + 1) 
	begin
	rule rl_from_nand_write_data_register_plane2 (rg_op_state[1] == READ_PAGE_CACHE);
		let data <- nand_target_plane2[i].portA.response.get( ) ;
		data_reg[2].portA.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: data_reg_address[2], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[2] < `VALID_COL_ADDR)
			data_reg_address[2] <= data_reg_address[2] + 1;
		else
			data_reg_address[2]     <= 'h0 ;
	endrule
	end
	
	//Lun1 Plane1
	for(Integer i=0 ; i < ((2**`PAGE_WIDTH)*(2**`BLOCK_WIDTH)) ; i = i + 1) 
	//for(Integer i=0 ; i < 4 ; i = i + 1) 
	begin
	rule rl_from_nand_write_data_register_plane3 (rg_op_state[1] == READ_PAGE_CACHE);
		let data <- nand_target_plane3[i].portA.response.get( ) ;
		data_reg[3].portA.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: data_reg_address[3], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[3] < `VALID_COL_ADDR)
			data_reg_address[3] <= data_reg_address[3] + 1;
		else
			data_reg_address[3]     <= 'h0 ;
	endrule
	end
	
	//Done writing into all data registers through port A
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                     END OF WRITE INTO DATA REGISTER DATA READ FROM TARGET                                                               ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                     READ FROM DATA REGISTER TO WRITE INTO CACHE REGISTER                                                              ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//Here rules to read from the data register to the cache register.
	//Lun0 Plane0
	rule rl_read_data_register_to_cache_reg_plane0 (rg_op_state[0] == READ_PAGE_CACHE_LAST && flag_read_queued_plane[0] == 1'b1);
		data_reg[0].portB.request.put ( BRAMRequest{
					        	    write : False, 
					        	    address: data_reg_address[0], 
					        	    datain : ? , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[0] < `VALID_COL_ADDR)
		begin
			data_reg_address[0] <= data_reg_address[0] + 1;
			flag_RDY[0]  <= 1'b0 ;
			flag_ARDY[0] <= 1'b0 ;
		end
		else
		begin
			data_reg_address[0]               <= 'h0 ;
			flag_read_queued_plane[0]         <= 1'b0 ;
			flag_RDY[0]                       <= 1'b1 ; // Lower the ready flag since page will end here.
			flag_read_queued_plane_last[0]    <= 1'b1 ; // This flag is needed to reset the operation to read page cache .
			if(wait_read_page_cache[0] == 0)
				flag_ARDY[0]                      <= 1'b1 ;
			else
				flag_ARDY[0]                      <= 1'b0 ;
		end
	endrule
	//Lun0 Plane1
	rule rl_read_data_register_to_cache_reg_plane1 (rg_op_state[0] == READ_PAGE_CACHE_LAST && flag_read_queued_plane[1] == 1'b1);
		data_reg[1].portB.request.put ( BRAMRequest{
					        	    write : False, 
					        	    address: data_reg_address[1], 
					        	    datain : ? , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[1] < `VALID_COL_ADDR)
		begin
			data_reg_address[1] <= data_reg_address[1] + 1;
			flag_RDY[1]  <= 1'b0 ;
			flag_ARDY[1] <= 1'b0 ;
		end
		else
		begin
			data_reg_address[1]               <= 'h0 ;
			flag_read_queued_plane[1]         <= 1'b0 ;
			flag_RDY[1]                       <= 1'b1 ; // Lower the ready flag since page will end here.
			flag_read_queued_plane_last[1]    <= 1'b1 ; // This flag is needed to reset the operation to read page cache .
			if(wait_read_page_cache[0] == 0)
				flag_ARDY[1]                      <= 1'b1 ;
			else
				flag_ARDY[1]                      <= 1'b0 ;
		end
	endrule
	//Lun1 Plane0
	rule rl_read_data_register_to_cache_reg_plane2 (rg_op_state[1] == READ_PAGE_CACHE_LAST && flag_read_queued_plane[2] == 1'b1);
		data_reg[2].portB.request.put ( BRAMRequest{
					        	    write : False, 
					        	    address: data_reg_address[2], 
					        	    datain : ? , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[2] < `VALID_COL_ADDR)
		begin
			data_reg_address[2] <= data_reg_address[2] + 1;
			flag_RDY[2]  <= 1'b0 ;
			flag_ARDY[2] <= 1'b0 ;
		end
		else
		begin
			data_reg_address[2]               <= 'h0 ;
			flag_read_queued_plane[2]         <= 1'b0 ;
			flag_RDY[2]                       <= 1'b1 ; // Lower the ready flag since page will end here.
			flag_read_queued_plane_last[2]    <= 1'b1 ; // This flag is needed to reset the operation to read page cache .
			if(wait_read_page_cache[1] == 0)
				flag_ARDY[2]                      <= 1'b1 ;
			else
				flag_ARDY[2]                      <= 1'b0 ;
		end
	endrule
	//Lun1 Plane1
	rule rl_read_data_register_to_cache_reg_plane3 (rg_op_state[1] == READ_PAGE_CACHE_LAST && flag_read_queued_plane[3] == 1'b1);
		data_reg[3].portB.request.put ( BRAMRequest{
					        	    write : False, 
					        	    address: data_reg_address[3], 
					        	    datain : ? , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[3] < `VALID_COL_ADDR)
		begin
			data_reg_address[3] <= data_reg_address[3] + 1;
			flag_RDY[3]  <= 1'b0 ;
			flag_ARDY[3] <= 1'b0 ;
		end
		else
		begin
			data_reg_address[3]               <= 'h0 ;
			flag_read_queued_plane[3]         <= 1'b0 ;
			flag_RDY[3]                       <= 1'b1 ; // Lower the ready flag since page will end here.
			flag_read_queued_plane_last[3]    <= 1'b1 ; // This flag is needed to reset the operation to read page cache .
			if(wait_read_page_cache[1] == 0)
				flag_ARDY[3]                      <= 1'b1 ;
			else
				flag_ARDY[3]                      <= 1'b0 ;
		end
	endrule
	//Done writing into all data registers through port B. Now write this into corresponding cache register.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                END OF READ FROM DATA REGISTER TO WRITE INTO CACHE REGISTER                                                              ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                WRITE INTO CACHE REGISTER DATA READ FROM DATA REGISTER                                                                   ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Here writing data from data reg to cache reg.
	//Lun0 Plane0
	rule rl_read_data_reg_write_cache_reg_plane0 (rg_op_state[0] == READ_PAGE_CACHE_LAST) ; // fired 1 cycle after rule "rl_read_data_register" is called.
        	let data <- data_reg[0].portB.response.get( ) ;
                cache_reg[0].portB.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: cache_reg_address[0], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					       ) ;
		if(cache_reg_address[0] < `VALID_COL_ADDR)
		begin
			cache_reg_address[0] <= cache_reg_address[0] + 1;
		end
		else
		begin
			cache_reg_address[0] <= 'h0 ;
		end
	endrule
	//Lun0 Plane1
	rule rl_read_data_reg_write_cache_reg_plane1 (rg_op_state[0] == READ_PAGE_CACHE_LAST) ; // fired 1 cycle after rule "rl_read_data_register" is called.
        	let data <- data_reg[1].portB.response.get( ) ;
                cache_reg[1].portB.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: cache_reg_address[1], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					       ) ;
		if(cache_reg_address[1] < `VALID_COL_ADDR)
		begin
			cache_reg_address[1] <= cache_reg_address[1] + 1;
		end
		else
		begin
			cache_reg_address[1] <= 'h0 ;
		end
	endrule
	//Lun1 Plane0
	rule rl_read_data_reg_write_cache_reg_plane2 (rg_op_state[1] == READ_PAGE_CACHE_LAST) ; // fired 1 cycle after rule "rl_read_data_register" is called.
        	let data <- data_reg[2].portB.response.get( ) ;
                cache_reg[2].portB.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: cache_reg_address[2], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					       ) ;
		if(cache_reg_address[2] < `VALID_COL_ADDR)
		begin
			cache_reg_address[2] <= cache_reg_address[2] + 1;
		end
		else
		begin
			cache_reg_address[2] <= 'h0 ;
		end
	endrule
	//Lun1 Plane1
	rule rl_read_data_reg_write_cache_reg_plane3 (rg_op_state[1] == READ_PAGE_CACHE_LAST) ; // fired 1 cycle after rule "rl_read_data_register" is called.
        	let data <- data_reg[3].portB.response.get( ) ;
                cache_reg[3].portB.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: cache_reg_address[3], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					       ) ;
		if(cache_reg_address[3] < `VALID_COL_ADDR)
		begin
			cache_reg_address[3] <= cache_reg_address[3] + 1;
		end
		else
		begin
			cache_reg_address[3] <= 'h0 ;
		end
	endrule
	//Done writing data from the data register to coresponding cache register
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                        END OF WRITE INTO CACHE REGISTER DATA READ FROM DATA REGISTER                                                    ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                   PROGRAM THE TARGET FROM CACHE REGISTER                                                                ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Rule to PROGRAM the pages (1 page per plane). Rules are split for each plane, since each plane has to read parallely .
	//Lun0 plane 0
	rule rl_prgm_target_plane0_from_cache (rg_op_state[0] == PROGRAM) ;  
		let data <- cache_reg[0].portB.response.get( ) ;
//Since we have nly 2 blocks per plane, only one index is used to select block address(i.e queued_block[plane][1]).On putting all blocks per plane,uncomment the following line and comment out the latter line.
		nand_target_plane0[{queued_block[0],queued_page[0]}].portA.request.put ( BRAMRequest{
		//nand_target_plane0[{queued_block[0][1],queued_page[0][0]}].portA.request.put ( BRAMRequest{
					        	    						write : True, 
					        	    						address: target_col_address[0], 
					        	    						datain : data , 
					        	    						responseOnWrite : False} 
					      			       			      ) ;	
		if(target_col_address[0] < `VALID_COL_ADDR)
		begin
			target_col_address[0] <= target_col_address[0]+1;
		end
		else
		begin
			target_col_address[0] <= 'h0;
		end
	endrule
	//Lun0 plane 1
	rule rl_prgm_target_plane1_from_cache (rg_op_state[0] == PROGRAM) ; 
		let data <- cache_reg[1].portB.response.get( ) ;
		nand_target_plane1[{queued_block[1],queued_page[1]}].portA.request.put ( BRAMRequest{
		//nand_target_plane1[{queued_block[1][1],queued_page[1][0]}].portA.request.put ( BRAMRequest{
					        	    						write : True, 
					        	    						address: target_col_address[1], 
					        	    						datain : data , 
					        	    						responseOnWrite : False} 
					      			       			      ) ;	
		if(target_col_address[1] < `VALID_COL_ADDR)
		begin
			target_col_address[1] <= target_col_address[1]+1;
		end
		else
		begin
			target_col_address[1] <= 'h0;
		end
	endrule
	//Lun1 plane 0
	rule rl_prgm_target_plane2_from_cache (rg_op_state[1] == PROGRAM) ;  
		let data <- cache_reg[2].portB.response.get( ) ;
		nand_target_plane2[{queued_block[2],queued_page[2]}].portA.request.put ( BRAMRequest{
		//nand_target_plane2[{queued_block[2][1],queued_page[2][0]}].portA.request.put ( BRAMRequest{
					        	    						write : True, 
					        	    						address: target_col_address[2], 
					        	    						datain : data , 
					        	    						responseOnWrite : False} 
					      			       			      ) ;	
		if(target_col_address[2] < `VALID_COL_ADDR)
		begin
			target_col_address[2] <= target_col_address[2]+1;
		end
		else
		begin
			target_col_address[2] <= 'h0;
		end
	endrule
	//Lun1 plane 1
	rule rl_prgm_target_plane3_from_cache (rg_op_state[1] == PROGRAM) ;  
		let data <- cache_reg[3].portB.response.get( ) ;
		nand_target_plane3[{queued_block[3],queued_page[3]}].portA.request.put ( BRAMRequest{
		//nand_target_plane3[{queued_block[3][1],queued_page[3][0]}].portA.request.put ( BRAMRequest{
					        	    						write : True, 
					        	    						address: target_col_address[3], 
					        	    						datain : data , 
					        	    						responseOnWrite : False} 
					      			       			      ) ;	
		if(target_col_address[3] < `VALID_COL_ADDR)
		begin
			target_col_address[3] <= target_col_address[3]+1;
		end
		else
		begin
			target_col_address[3] <= 'h0;
		end
	endrule
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                                    END OF PROGRAM TARGET                                                                ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                                    READ FROM CACHE REGISTER                                                             ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Lun0 plane0
	//Need to check flag_ARDY in rule while program_page_cache, since if the earlier write from data register to target is incomplete(flag_ARDY = 0), then there will be collission if we try to write from cache register to data regiter when already a write is happening from data register to target.
	rule rl_read_cache_reg_plane0(((rg_op_state[0] == PROGRAM)||(rg_op_state[0] == PROGRAM_PAGE_CACHE && flag_prgm_last[0] == 0)) && flag_prgm_queued_plane[0] == 1'b1) ; 
                cache_reg[0].portB.request.put ( BRAMRequest{
					        	    write : False, 
					        	    address: cache_reg_address[0], 
					        	    datain : ? , 
					        	    responseOnWrite : False} 
					      	) ;
		if(cache_reg_address[0] < `VALID_COL_ADDR)
		begin
			cache_reg_address[0] <= cache_reg_address[0] + 1;
			flag_RDY[0]  <= 1'b0 ;
			flag_ARDY[0] <= 1'b0 ;
		end
		else
		begin
			cache_reg_address[0]       <= 'h0 ;
			if(rg_op_state[0] == PROGRAM)
			begin
				flag_prgm_queued_plane[0]  <= 1'b0 ;                    // In this case rg_op_state will be disabled in busy rule in next cycle.
				flag_RDY[0]                <= 1'b1 ;
				flag_ARDY[0]               <= 1'b1 ;  
			end 
			// If it is program page cache then need to do program page cache last after this, hence need to update reg_op_state to page cache last. 
			else
				flag_prgm_last[0] <= 1'b1 ;          
		end
	endrule
	//Lun0 Plane1
	rule rl_read_cache_reg_plane1(((rg_op_state[0] == PROGRAM)||(rg_op_state[0] == PROGRAM_PAGE_CACHE && flag_prgm_last[1] == 0)) && flag_prgm_queued_plane[1] == 1'b1); 
                cache_reg[1].portB.request.put ( BRAMRequest{
					        	    write : False, 
					        	    address: cache_reg_address[1], 
					        	    datain : ? , 
					        	    responseOnWrite : False} 
					      	) ;
		if(cache_reg_address[1] < `VALID_COL_ADDR)
		begin
			cache_reg_address[1] <= cache_reg_address[1] + 1;
			flag_RDY[1]  <= 1'b0 ;
			flag_ARDY[1] <= 1'b0 ;
		end
		else
		begin
			cache_reg_address[1]       <= 'h0 ;
			if(rg_op_state[0] == PROGRAM)
			begin
				flag_prgm_queued_plane[1]  <= 1'b0 ;
				flag_RDY[1]                <= 1'b1 ;
				flag_ARDY[1]               <= 1'b1 ;
			end
			// If it is program page cache then need to do program page cache last after this, hence need to update reg_op_state to page cache last.  
			else
				flag_prgm_last[1] <= 1'b1 ;         
		end
	endrule
	//Lun1 Plane0
	rule rl_read_cache_reg_plane2(((rg_op_state[1] == PROGRAM)||(rg_op_state[1] == PROGRAM_PAGE_CACHE && flag_prgm_last[2] == 0)) && flag_prgm_queued_plane[2] == 1'b1); 
                cache_reg[2].portB.request.put ( BRAMRequest{
					        	    write : False, 
					        	    address: cache_reg_address[2], 
					        	    datain : ? , 
					        	    responseOnWrite : False} 
					      	) ;
		if(cache_reg_address[2] < `VALID_COL_ADDR)
		begin
			cache_reg_address[2] <= cache_reg_address[2] + 1;
			flag_RDY[2]  <= 1'b0 ;
			flag_ARDY[2] <= 1'b0 ;
		end
		else
		begin
			cache_reg_address[2]       <= 'h0 ;
			if(rg_op_state[1] == PROGRAM)
			begin
				flag_prgm_queued_plane[2]  <= 1'b0 ;
				flag_RDY[2]                <= 1'b1 ;
				flag_ARDY[2]               <= 1'b1 ;
			end
			// If it is program page cache then need to do program page cache last after this, hence need to update reg_op_state to page cache last.  
			else
				flag_prgm_last[2] <= 1'b1 ;         
		end
	endrule
	//Lun1 Plane1
	rule rl_read_cache_reg_plane3(((rg_op_state[1] == PROGRAM)||(rg_op_state[1] == PROGRAM_PAGE_CACHE && flag_prgm_last[3] == 0)) && flag_prgm_queued_plane[3] == 1'b1); 
                cache_reg[3].portB.request.put ( BRAMRequest{
					        	    write : False, 
					        	    address: cache_reg_address[3], 
					        	    datain : ? , 
					        	    responseOnWrite : False} 
					      	) ;
		if(cache_reg_address[3] < `VALID_COL_ADDR)
		begin
			cache_reg_address[3] <= cache_reg_address[3] + 1;
			flag_RDY[3]  <= 1'b0 ;
			flag_ARDY[3] <= 1'b0 ;
		end
		else
		begin
			cache_reg_address[3]       <= 'h0 ;
			if(rg_op_state[1] == PROGRAM)
			begin
				flag_prgm_queued_plane[3]  <= 1'b0 ;
				flag_RDY[3]                <= 1'b1 ;
				flag_ARDY[3]               <= 1'b1 ;
			end
			// If it is program page cache then need to do program page cache last after this, hence need to update reg_op_state to page cache last.  
			else
				flag_prgm_last[3] <= 1'b1 ;         
		end
	endrule
	//Done reading fromm all cache registers through port B
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                       END OF READ FROM CACHE REGISTER                                                                   ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                           PROGRAM TO DATA REGISTER,DATA READ FROM CACHE REGISTER                                                        ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Here rules to write from the cache to data register. 
	//Lun0 Plane0
	rule rl_from_cache_write_data_register_plane0 (rg_op_state[0] == PROGRAM_PAGE_CACHE);
		let data <- cache_reg[0].portB.response.get( ) ;
		data_reg[0].portB.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: data_reg_address[0], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[0] < `VALID_COL_ADDR)
		begin
			data_reg_address[0] <= data_reg_address[0] + 1;
		end
		else
		begin
			data_reg_address[0]     <= 'h0 ;
		end
	endrule
	//Lun0 Plane1
	rule rl_from_cache_write_data_register_plane1 (rg_op_state[0] == PROGRAM_PAGE_CACHE);
		let data <- cache_reg[1].portB.response.get( ) ;
		data_reg[1].portB.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: data_reg_address[1], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[1] < `VALID_COL_ADDR)
		begin
			data_reg_address[1] <= data_reg_address[1] + 1;
		end
		else
		begin
			data_reg_address[1]     <= 'h0 ;
		end
	endrule
	//Lun1 Plane0
	rule rl_from_cache_write_data_register_plane2 (rg_op_state[1] == PROGRAM_PAGE_CACHE);
		let data <- cache_reg[2].portB.response.get( ) ;
		data_reg[2].portB.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: data_reg_address[2], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[2] < `VALID_COL_ADDR)
		begin
			data_reg_address[2] <= data_reg_address[2] + 1;
		end
		else
		begin
			data_reg_address[2] <= 'h0 ;
		end
	endrule
	//Lun1 Plane1
	rule rl_from_cache_write_data_register_plane3 (rg_op_state[1] == PROGRAM_PAGE_CACHE);
		let data <- cache_reg[3].portB.response.get( ) ;
		data_reg[3].portB.request.put ( BRAMRequest{
					        	    write : True, 
					        	    address: data_reg_address[3], 
					        	    datain : data , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[3] < `VALID_COL_ADDR)
		begin
			data_reg_address[3] <= data_reg_address[3] + 1;
		end
		else
		begin
			data_reg_address[3]     <= 'h0 ;
		end
	endrule
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                    END OF PROGRAM TO DATA REGISTER,DATA READ FROM CACHE REGISTER                                                        ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                 READ FROM DATA REGISTER TO PROGRAM THE TARGET                                                           ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Lun0 Plane0
	rule rl_from_data_register_to_target_plane0 (rg_op_state[0] == PROGRAM_PAGE_CACHE_LAST && flag_prgm_queued_plane_last[0] == 1'b1);
		data_reg[0].portA.request.put ( BRAMRequest{
					        	    write : False, 
					        	    address: data_reg_address[0], 
					        	    datain : ? , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[0] < `VALID_COL_ADDR)
		begin
			data_reg_address[0] <= data_reg_address[0] + 1;
			//If next program page cache is waiting while data register is still copying into target, enable rdy indicating data register not yet available.
			if(wait_program_page_cache[0] == 1 && flag_prgm_queued_plane[0] == 1)
				flag_RDY[0] <= 1'b0;
		end
		else
		begin
			data_reg_address[0]        <= 'h0 ;
			flag_prgm_queued_plane_last[0]  <= 1'b0 ;
			//If next program page cache is waiting enable Ardy.
			if(wait_program_page_cache[0] == 1 && flag_prgm_queued_plane[0] == 1)
			flag_ARDY[0]               <= 1'b0 ;
			else
			flag_ARDY[0]               <= 1'b1 ;
		end
	endrule
	//Lun0 Plane1
	rule rl_from_data_register_to_target_plane1 (rg_op_state[0] == PROGRAM_PAGE_CACHE_LAST && flag_prgm_queued_plane_last[1] == 1'b1);
		data_reg[1].portA.request.put ( BRAMRequest{
					        	    write : False, 
					        	    address: data_reg_address[1], 
					        	    datain : ? , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[1] < `VALID_COL_ADDR)
		begin
			data_reg_address[1] <= data_reg_address[1] + 1;
			//If next program page cache is waiting while data register is still copying into target, enable rdy indicating data register not yet available.
			if(wait_program_page_cache[0] == 1 && flag_prgm_queued_plane[1] == 1)
				flag_RDY[1] <= 1'b0;
		end
		else
		begin
			data_reg_address[1]        <= 'h0 ;
			flag_prgm_queued_plane_last[1]  <= 1'b0 ;
			if(wait_program_page_cache[0] == 1 && flag_prgm_queued_plane[1] == 1)
			flag_ARDY[1]               <= 1'b0 ;
			else
			flag_ARDY[1]               <= 1'b1 ;
		end
	endrule
	//Lun1 Plane2
	rule rl_from_data_register_to_target_plane2 (rg_op_state[1] == PROGRAM_PAGE_CACHE_LAST && flag_prgm_queued_plane_last[2] == 1'b1);
		data_reg[2].portA.request.put ( BRAMRequest{
					        	    write : False, 
					        	    address: data_reg_address[2], 
					        	    datain : ? , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[2] < `VALID_COL_ADDR)
		begin
			data_reg_address[2] <= data_reg_address[2] + 1;
			//If next program page cache is waiting while data register is still copying into target, enable rdy indicating data register not yet available.
			if(wait_program_page_cache[0] == 1 && flag_prgm_queued_plane[0] == 1)
				flag_RDY[2] <= 1'b0;
		end
		else
		begin
			data_reg_address[2]        <= 'h0 ;
			flag_prgm_queued_plane_last[2]  <= 1'b0 ;
			if(wait_program_page_cache[1] == 1 && flag_prgm_queued_plane[2] == 1)
			flag_ARDY[2]               <= 1'b0 ;
			else
			flag_ARDY[2]               <= 1'b1 ;
		end
	endrule
	//Lun1 Plane1
	rule rl_from_data_register_to_target_plane3 (rg_op_state[1] == PROGRAM_PAGE_CACHE_LAST && flag_prgm_queued_plane_last[3] == 1'b1);
		data_reg[3].portA.request.put ( BRAMRequest{
					        	    write : False, 
					        	    address: data_reg_address[3], 
					        	    datain : ? , 
					        	    responseOnWrite : False} 
					      ) ;
		if(data_reg_address[3] < `VALID_COL_ADDR)
		begin
			data_reg_address[3] <= data_reg_address[3] + 1;
			//If next program page cache is waiting while data register is still copying into target, enable rdy indicating data register not yet available.
			if(wait_program_page_cache[0] == 1 && flag_prgm_queued_plane[0] == 1)
				flag_RDY[3] <= 1'b0;
		end
		else
		begin
			data_reg_address[3]        <= 'h0 ;
			flag_prgm_queued_plane_last[3]  <= 1'b0 ;
			if(wait_program_page_cache[1] == 1 && flag_prgm_queued_plane[3] == 1)
			flag_ARDY[3]               <= 1'b0 ;
			else
			flag_ARDY[3]               <= 1'b1 ;
		end
	endrule
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                           END OF READ FROM DATA REGISTER TO PROGRAM THE TARGET                                                          ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                PROGRAM THE TARGET WITH DATA FROM DATA REGISTER                                                          ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Lun0 plane 0
	rule rl_prgm_target_plane0_from_data_reg (rg_op_state[0] == PROGRAM_PAGE_CACHE_LAST) ;  
		let data <- data_reg[0].portA.response.get( ) ;
		nand_target_plane0[{queued_block[0],queued_page[0]}].portA.request.put ( BRAMRequest{
		//nand_target_plane0[{queued_block[0][1],queued_page[0][0]}].portA.request.put ( BRAMRequest{
					        	    						write : True, 
					        	    						address: target_col_address[0], 
					        	    						datain : data , 
					        	    						responseOnWrite : False} 
					      			       			      ) ;	
		if(target_col_address[0] < `VALID_COL_ADDR)
		begin
			target_col_address[0] <= target_col_address[0]+1;
		end
		else
		begin
			target_col_address[0] <= 'h0;
		end
	endrule
	//Lun0 plane 1
	rule rl_prgm_target_plane1_from_data_reg (rg_op_state[0] == PROGRAM_PAGE_CACHE_LAST) ; 
		let data <- data_reg[1].portA.response.get( ) ;
		nand_target_plane1[{queued_block[1],queued_page[1]}].portA.request.put ( BRAMRequest{
		//nand_target_plane1[{queued_block[1][1],queued_page[1][0]}].portA.request.put ( BRAMRequest{
					        	    						write : True, 
					        	    						address: target_col_address[1], 
					        	    						datain : data , 
					        	    						responseOnWrite : False} 
					      			       			      ) ;	
		if(target_col_address[1] < `VALID_COL_ADDR)
		begin
			target_col_address[1] <= target_col_address[1]+1;
		end
		else
		begin
			target_col_address[1] <= 'h0;
		end
	endrule
	//Lun1 plane 0
	rule rl_prgm_target_plane2_from_data_reg (rg_op_state[1] == PROGRAM_PAGE_CACHE_LAST) ;  
		let data <- data_reg[2].portA.response.get( ) ;
		nand_target_plane2[{queued_block[2],queued_page[2]}].portA.request.put ( BRAMRequest{
		//nand_target_plane2[{queued_block[2][1],queued_page[2][0]}].portA.request.put ( BRAMRequest{
					        	    						write : True, 
					        	    						address: target_col_address[2], 
					        	    						datain : data , 
					        	    						responseOnWrite : False} 
					      			       			      ) ;	
		if(target_col_address[2] < `VALID_COL_ADDR)
		begin
			target_col_address[2] <= target_col_address[2]+1;
		end
		else
		begin
			target_col_address[2] <= 'h0;
		end
	endrule
	//Lun1 plane 1
	rule rl_prgm_target_plane3_from_data_reg (rg_op_state[1] == PROGRAM_PAGE_CACHE_LAST) ;  
		let data <- data_reg[3].portA.response.get( ) ;
		nand_target_plane3[{queued_block[3],queued_page[3]}].portA.request.put ( BRAMRequest{
		//nand_target_plane3[{queued_block[3][1],queued_page[3][0]}].portA.request.put ( BRAMRequest{
					        	    						write : True, 
					        	    						address: target_col_address[3], 
					        	    						datain : data , 
					        	    						responseOnWrite : False} 
					      			       			      ) ;	
		if(target_col_address[3] < `VALID_COL_ADDR)
		begin
			target_col_address[3] <= target_col_address[3]+1;
		end
		else
		begin
			target_col_address[3] <= 'h0;
		end
	endrule
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                          END OF PROGRAM THE TARGET WITH DATA FROM DATA REGISTER                                                         ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                                 BLOCK ERASE                                                                             ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//In the block pointed by block addr we need to erase pages on by one,hence need a reg to point n increment to each page.
	
	Vector#(`TOTAL_PLANE,Reg#(Bit#(`PAGE_WIDTH))) erase_page_addr    <- replicateM(mkReg(0));
	//Reg#(Bit#(1)) erase_page_addr    <- mkReg(0);
	
	//Lun0 plane 0
	rule rl_erase_target_plane0 (rg_op_state[0] == ERASE && flag_erase_queued_plane[0] == 1'b1) ;  
		nand_target_plane0[{queued_block[0],erase_page_addr[0]}].portA.request.put ( BRAMRequest{
		//nand_target_plane0[{queued_block[0][1],erase_page_addr[0]}].portA.request.put ( BRAMRequest{
					        	    					write : True, 
					        	    					address: target_col_address[0], 
					        	    					datain :  'hFF, 
					        	    					responseOnWrite : False} 
					      			       	   	) ;	
		if(target_col_address[0] < `VALID_COL_ADDR)
		begin
			target_col_address[0] <= target_col_address[0]+1;
			flag_RDY[0]  <= 1'b0 ;
			flag_ARDY[0] <= 1'b0 ;
		end
		else
		begin
			target_col_address[0]      <= 'h0;
			//if(erase_page_addr == 'h1)
			if(erase_page_addr[0] == ~page_max)
			begin
				erase_page_addr[0]            <= 'h0 ;
				flag_erase_queued_plane[0] <= 1'b0 ;
				flag_RDY[0]                <= 1'b1 ; // Lower the ready flag since page will end here.
				flag_ARDY[0]               <= 1'b1 ;
			end
			else
				erase_page_addr[0] <= erase_page_addr[0] + 'h1 ;
		end
	endrule
	
	//Lun0 plane 1
	rule rl_erase_target_plane1 (rg_op_state[0] == ERASE && flag_erase_queued_plane[1] == 1'b1) ;  
		nand_target_plane1[{queued_block[1],erase_page_addr[1]}].portA.request.put ( BRAMRequest{
		//nand_target_plane1[{queued_block[1][1],erase_page_addr[1]}].portA.request.put ( BRAMRequest{
					        	    					write : True, 
					        	    					address: target_col_address[1], 
					        	    					datain :  'hFF, 
					        	    					responseOnWrite : False} 
					      			       		) ;	
		if(target_col_address[1] < `VALID_COL_ADDR)
		begin
			target_col_address[1] <= target_col_address[1]+1;
			flag_RDY[1]  <= 1'b0 ;
			flag_ARDY[1] <= 1'b0 ;
		end
		else
		begin
			target_col_address[1]      <= 'h0;
			//if(erase_page_addr == 'h1)
			if(erase_page_addr[1] == ~page_max)
			begin
				erase_page_addr[1]            <= 'h0 ;
				flag_erase_queued_plane[1] <= 1'b0 ;
				flag_RDY[1]                <= 1'b1 ; // Lower the ready flag since page will end here.
				flag_ARDY[1]               <= 1'b1 ;
			end
			else
				erase_page_addr[1] <= erase_page_addr[1] + 'h1 ;
		end
	endrule
	
	//Lun1 plane 0
	rule rl_erase_target_plane2 (rg_op_state[1] == ERASE && flag_erase_queued_plane[2] == 1'b1) ;  
		nand_target_plane2[{queued_block[2],erase_page_addr[2]}].portA.request.put ( BRAMRequest{
		//nand_target_plane2[{queued_block[2][1],erase_page_addr[2]}].portA.request.put ( BRAMRequest{
					        	    					write : True, 
					        	    					address: target_col_address[2], 
					        	    					datain :  'hFF, 
					        	    					responseOnWrite : False} 
					      			       		) ;	
		if(target_col_address[2] < `VALID_COL_ADDR)
		begin
			target_col_address[2] <= target_col_address[2]+1;
			flag_RDY[2]  <= 1'b0 ;
			flag_ARDY[2] <= 1'b0 ;
		end
		else
		begin
			target_col_address[2]      <= 'h0;
			//if(erase_page_addr == 'h1)
			if(erase_page_addr[2] == ~page_max)
			begin
				erase_page_addr[2]            <= 'h0 ;
				flag_erase_queued_plane[2] <= 1'b0 ;
				flag_RDY[2]                <= 1'b1 ; // Lower the ready flag since page will end here.
				flag_ARDY[2]               <= 1'b1 ;
			end
			else
				erase_page_addr[2] <= erase_page_addr[2] + 'h1 ;
		end
	endrule
	
	//Lun1 plane 1
	rule rl_erase_target_plane3 (rg_op_state[1] == ERASE && flag_erase_queued_plane[3] == 1'b1) ;  
		nand_target_plane3[{queued_block[3],erase_page_addr[3]}].portA.request.put ( BRAMRequest{
		//nand_target_plane3[{queued_block[3][1],erase_page_addr[3]}].portA.request.put ( BRAMRequest{
					        	    					write : True, 
					        	    					address: target_col_address[3], 
					        	    					datain :  'hFF, 
					        	    					responseOnWrite : False} 
					      			       		) ;	
		if(target_col_address[3] < `VALID_COL_ADDR)
		begin
			target_col_address[3] <= target_col_address[3]+1;
			flag_RDY[3]  <= 1'b0 ;
			flag_ARDY[3] <= 1'b0 ;
		end
		else
		begin
			target_col_address[3]      <= 'h0;
			//if(erase_page_addr == 'h1)
			if(erase_page_addr[3] == ~page_max)
			begin
				erase_page_addr[3]            <= 'h0 ;
				flag_erase_queued_plane[3] <= 1'b0 ;
				flag_RDY[3]                <= 1'b1 ; // Lower the ready flag since page will end here.
				flag_ARDY[3]               <= 1'b1 ;
			end
			else
				erase_page_addr[3] <= erase_page_addr[3] + 'h1 ;
		end
	endrule
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////                                                 BLOCK ERASE END                                                                         ///////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




interface onfi_target_interface = fn_onfi_target_interface ( wr_onfi_ce_n, wr_onfi_cle, wr_onfi_ale, wr_onfi_we_n, wr_onfi_re_n, wr_onfi_wp_n, rg_data_to_nfc, wr_data_from_nfc, rg_t_ready_busy_n) ;

endmodule

endpackage
