package NFCTargetConnection ;

import Connectable :: * ;

import InterfaceNandFlashController :: * ;
import bsvmknand_model :: * ;

//(* mutually_exclusive = "rl_data_from_flash_0, rl_data_from_flash_1 " *)
	module mkConnection #( ONFiInterface onfi_ifc , Ifc_nand_model target_ifc ) ( Empty ) ;

	//	(* no_implicit_conditions, fire_when_enabled *)

		rule rl_ready_busy_0 ;
			onfi_ifc._ready_busy0_n_m (target_ifc.oRb_n) ;
		endrule
		
		rule rl_onfi_ce_0 ;
			target_ifc.iCe_n (onfi_ifc.onfi_ce0_n_) ;
		endrule
		
		rule rl_onfi_we_0 ;
			target_ifc.iClk_we_n (onfi_ifc.onfi_we_n_) ;
		endrule
		
		rule rl_onfi_re_0 ;
			target_ifc.iWr_Re_n (onfi_ifc.onfi_re_n_) ;
		endrule
		
		rule rl_onfi_wp_0 ;
			target_ifc.iWp_n (onfi_ifc.onfi_wp_n_) ;
		endrule

		rule rl_onfi_cle_0 ;
			target_ifc.iCle (onfi_ifc.onfi_cle_) ;
		endrule

		rule rl_onfi_ale_0 ;
			target_ifc.iAle (onfi_ifc.onfi_ale_) ;
		endrule
				
		rule rl_ready_busy_1 ;
			onfi_ifc._ready_busy1_n_m (target_ifc.oRb2_n) ;
		endrule
		
		rule rl_onfi_ce_1 ;
			target_ifc.iCe2_n (onfi_ifc.onfi_ce1_n_) ;
		endrule
		
		rule rl_onfi_we_1 ;
			target_ifc.iClk_we2_n (onfi_ifc.onfi_we_n_) ;
		endrule
		
		rule rl_onfi_re_1 ;
			target_ifc.iWr_Re2_n (onfi_ifc.onfi_re_n_) ;
		endrule
		
		rule rl_onfi_wp_1 ;
			target_ifc.iWp2_n (onfi_ifc.onfi_wp_n_) ;
		endrule

		rule rl_onfi_cle_1 ;
			target_ifc.iCle2 (onfi_ifc.onfi_cle_) ;
		endrule

		rule rl_onfi_ale_1 ;
			target_ifc.iAle2 (onfi_ifc.onfi_ale_) ;
		endrule
		
		rule r1_onfi_Eni;
			target_ifc.iENi (1'b1);
		endrule

		rule r1_onfi_Re_c;
			target_ifc.iRe_c (~onfi_ifc.onfi_re_n_);
		endrule	
	endmodule
endpackage
