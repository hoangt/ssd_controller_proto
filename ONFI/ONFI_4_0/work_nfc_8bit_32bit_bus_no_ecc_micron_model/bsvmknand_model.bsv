package bsvmknand_model;
// Bluespec wrapper, created by Import BVI Wizard
// Created on: Tue Apr 21 17:49:53 IST 2015
// Created by: bhargav
// Bluespec version: 2014.06.A 2014-06-24 33987


interface Ifc_nand_model;
	interface Inout#(Bit#(8)) data1;
	interface Inout#(Bit#(8)) data0;
	interface Inout#(Bool) iDqs;
	interface Inout#(Bool) iDqs_c;
	method Action iENi (bit _ENi);
	method Action iCe2_n (bit _Ce2_n);
	method Action iCle2 (bit _Cle2);
	method Action iAle2 (bit _Ale2);
	method Action iWr_Re2_n (bit _Wr_Re2_n);
	method Action iClk_we2_n (bit _Clk_we2_n);
	method Action iWp2_n (bit _iWp2_n);
	method Action iCle (bit _Cle);
	method Action iAle (bit _Ale);
	method Action iCe_n (bit _Ce_n);
	method Action iWr_Re_n (bit _Wr_Re_n);
	method Action iClk_we_n (bit _Clk_we_n);
	method Action iWp_n (bit _Wp_n);
	method Action iRe_c (bit _Re_c);
	method bit oRb_n ();
	method bit oRb2_n ();
	method Bool oENo ();
endinterface

import "BVI" nand_model =
module mknand_model  (Ifc_nand_model);

	parameter MAX_LUN_PER_TAR = 2;

//	default_clock no_clock;
	default_reset no_reset;

	ifc_inout data0(Dq_Io);
	ifc_inout data1(Dq_Io2);

	ifc_inout iDqs(Dqs);
	ifc_inout iDqs_c(Dqs_c);



	method iENi (ENi)
		 enable((*inhigh*) en1);
	method iCe2_n (Ce2_n)
		 enable((*inhigh*) en2);
	method iCle2 (Cle2)
		 enable((*inhigh*) en3);
	method iAle2 (Ale2)
		 enable((*inhigh*) en4);
	method iWr_Re2_n (Wr_Re2_n)
		 enable((*inhigh*) en5);
	method iClk_we2_n (Clk_We2_n)
		enable((*inhigh*) en6);
	method iWp2_n (Wp2_n)
		 enable((*inhigh*) en7);
	method iCle (Cle)
		 enable((*inhigh*) en9);
	method iAle (Ale)
		 enable((*inhigh*) en10);
	method iCe_n (Ce_n)
		 enable((*inhigh*) en11);
	method iWr_Re_n (Wr_Re_n)
		 enable((*inhigh*) en12);
	method iClk_we_n (Clk_We_n)
		enable((*inhigh*) en13);
	method iWp_n (Wp_n)
		 enable((*inhigh*) en14);
	method iRe_c (Re_c)
		 enable((*inhigh*) en15);
	method Rb_n /* 0:0 */ oRb_n ()
		;
	method Rb2_n /* 0:0 */ oRb2_n ()
		;
	method ENo oENo ()
		;

	schedule iENi C iENi;
	schedule iENi CF iCe2_n;
	schedule iENi CF iCle2;
	schedule iENi CF iAle2;
	schedule iENi CF iWr_Re2_n;
	schedule iENi CF iWp2_n;
	schedule iENi CF iCle;
	schedule iENi CF iAle;
	schedule iENi CF iCe_n;
	schedule iENi CF iWr_Re_n;
	schedule iENi CF iClk_we_n;
	schedule iENi CF iClk_we2_n;
	schedule iENi CF iWp_n;
	schedule iENi CF iRe_c;
	schedule oRb_n SB iENi;
	schedule oRb2_n SB iENi;
	schedule oENo SB iENi;
	schedule iCe2_n C iCe2_n;
	schedule iCe2_n CF iCle2;
	schedule iCe2_n CF iAle2;
	schedule iCe2_n CF iWr_Re2_n;
	schedule iCe2_n CF iWp2_n;
	schedule iCe2_n CF iCle;
	schedule iCe2_n CF iAle;
	schedule iCe2_n CF iCe_n;
	schedule iCe2_n CF iWr_Re_n;
	schedule iCe2_n CF iClk_we_n;
	schedule iCe2_n CF iClk_we2_n;
	schedule iCe2_n CF iWp_n;
	schedule iCe2_n CF iRe_c;
	schedule oRb_n SB iCe2_n;
	schedule oRb2_n SB iCe2_n;
	schedule oENo SB iCe2_n;
	schedule iCle2 C iCle2;
	schedule iCle2 CF iAle2;
	schedule iCle2 CF iWr_Re2_n;
	schedule iCle2 CF iWp2_n;
	schedule iCle2 CF iCle;
	schedule iCle2 CF iAle;
	schedule iCle2 CF iCe_n;
	schedule iCle2 CF iWr_Re_n;
	schedule iCle2 CF iClk_we_n;
	schedule iCle2 CF iClk_we2_n;
	schedule iCle2 CF iWp_n;
	schedule iCle2 CF iRe_c;
	schedule oRb_n SB iCle2;
	schedule oRb2_n SB iCle2;
	schedule oENo SB iCle2;
	schedule iAle2 C iAle2;
	schedule iAle2 CF iWr_Re2_n;
	schedule iAle2 CF iWp2_n;
	schedule iAle2 CF iCle;
	schedule iAle2 CF iAle;
	schedule iAle2 CF iCe_n;
	schedule iAle2 CF iWr_Re_n;
	schedule iAle2 CF iClk_we_n;
	schedule iAle2 CF iClk_we2_n;
	schedule iAle2 CF iWp_n;
	schedule iAle2 CF iRe_c;
	schedule oRb_n SB iAle2;
	schedule oRb2_n SB iAle2;
	schedule oENo SB iAle2;
	schedule iWr_Re2_n C iWr_Re2_n;
	schedule iWr_Re2_n CF iWp2_n;
	schedule iWr_Re2_n CF iCle;
	schedule iWr_Re2_n CF iAle;
	schedule iWr_Re2_n CF iCe_n;
	schedule iWr_Re2_n CF iWr_Re_n;
	schedule iWr_Re2_n CF iClk_we_n;
	schedule iWr_Re2_n CF iClk_we2_n;
	schedule iWr_Re2_n CF iWp_n;
	schedule iWr_Re2_n CF iRe_c;
	schedule oRb_n SB iWr_Re2_n;
	schedule oRb2_n SB iWr_Re2_n;
	schedule oENo SB iWr_Re2_n;
	schedule iWp2_n C iWp2_n;
	schedule iWp2_n CF iCle;
	schedule iWp2_n CF iAle;
	schedule iWp2_n CF iCe_n;
	schedule iWp2_n CF iWr_Re_n;
	schedule iWp2_n CF iClk_we_n;
	schedule iWp2_n CF iClk_we2_n;
	schedule iWp2_n CF iWp_n;
	schedule iWp2_n CF iRe_c;
	schedule oRb_n SB iWp2_n;
	schedule oRb2_n SB iWp2_n;
	schedule oENo SB iWp2_n;
	schedule iCle C iCle;
	schedule iCle CF iAle;
	schedule iCle CF iCe_n;
	schedule iCle CF iWr_Re_n;
	schedule iCle CF iClk_we_n;
	schedule iCle CF iClk_we2_n;
	schedule iCle CF iWp_n;
	schedule iCle CF iRe_c;
	schedule oRb_n SB iCle;
	schedule oRb2_n SB iCle;
	schedule oENo SB iCle;
	schedule iAle C iAle;
	schedule iAle CF iCe_n;
	schedule iAle CF iWr_Re_n;
	schedule iAle CF iClk_we_n;
	schedule iAle CF iClk_we2_n;
	schedule iAle CF iWp_n;
	schedule iAle CF iRe_c;
	schedule oRb_n SB iAle;
	schedule oRb2_n SB iAle;
	schedule oENo SB iAle;
	schedule iCe_n C iCe_n;
	schedule iCe_n CF iWr_Re_n;
	schedule iCe_n CF iClk_we_n;
	schedule iCe_n CF iClk_we2_n;
	schedule iCe_n CF iWp_n;
	schedule iCe_n CF iRe_c;
	schedule oRb_n SB iCe_n;
	schedule oRb2_n SB iCe_n;
	schedule oENo SB iCe_n;
	schedule iWr_Re_n C iWr_Re_n;
	schedule iWr_Re_n CF iWp_n;
	schedule iWr_Re_n CF iRe_c;
	schedule iWr_Re_n CF iClk_we_n;
	schedule iWr_Re_n CF iClk_we2_n;
	schedule oRb_n SB iWr_Re_n;
	schedule oRb2_n SB iWr_Re_n;
	schedule oENo SB iWr_Re_n;
	schedule iClk_we_n C iClk_we_n;
	schedule iClk_we_n CF iWp_n;
	schedule iClk_we_n CF iRe_c;
	schedule iClk_we_n CF iWr_Re_n;
	schedule iClk_we_n CF iClk_we2_n;
	schedule oRb_n SB iClk_we_n;
	schedule oRb2_n SB iClk_we_n;
	schedule oENo SB iClk_we_n;
	schedule iClk_we2_n C iClk_we2_n;
	schedule iClk_we2_n CF iWp_n;
	schedule iClk_we2_n CF iRe_c;
	schedule iClk_we2_n CF iWr_Re_n;
	schedule iClk_we2_n CF iClk_we_n;
	schedule oRb_n SB iClk_we2_n;
	schedule oRb2_n SB iClk_we2_n;
	schedule oENo SB iClk_we2_n;
	schedule iWp_n C iWp_n;
	schedule iWp_n CF iRe_c;
	schedule oRb_n SB iWp_n;
	schedule oRb2_n SB iWp_n;
	schedule oENo SB iWp_n;
	schedule iRe_c C iRe_c;
	schedule oRb_n SB iRe_c;
	schedule oRb2_n SB iRe_c;
	schedule oENo SB iRe_c;
	schedule oRb_n CF oRb_n;
	schedule oRb_n CF oRb2_n;
	schedule oRb_n CF oENo;
	schedule oRb2_n CF oRb2_n;
	schedule oRb2_n CF oENo;
	schedule oENo CF oENo;
endmodule

endpackage
