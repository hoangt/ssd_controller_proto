`timescale 1ps / 100fs
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.12.2014 11:59:22
// Design Name: 
// Module Name: tb_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_top(
    );
   parameter SIMULATION            = "TRUE";

   parameter SIM_BYPASS_INIT_CAL   = "FAST";
    
    reg clk;
    reg reset;
    reg sys_clk_p;
    initial
    begin
        clk <= 0;
        sys_clk_p <= 0;
        reset <= 1;
        #80000 reset <= 0;
    end
    
    
    always @(*)
    begin
        #4000 clk <= ~clk;
    end
    
    always #2500 sys_clk_p <= ~sys_clk_p;
    
    design_top design_top_i (
        .CLK (clk),
        .RESET (reset),
        .sys_clk_p(sys_clk_p),
        .sys_clk_n(~sys_clk_p)
        );
    
endmodule
