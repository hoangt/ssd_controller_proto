`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.12.2014 19:20:53
// Design Name: 
// Module Name: design_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module design_top(
		  input CLK,
		  input RESET,
		  input sys_clk_p,
		  input sys_clk_n
		  );
   
   
   wire [127:0]		axi_datamover_0_m_axis_mm2s_tdata_pin;
   wire 		axi_datamover_0_m_axis_mm2s_tlast_pin;
   wire 		axi_datamover_0_m_axis_mm2s_tready_pin;
   wire 		axi_datamover_0_m_axis_mm2s_tvalid_pin;
   wire [71:0]		axi_datamover_0_s_axis_mm2s_cmd_tdata_pin;
   wire 		axi_datamover_0_s_axis_mm2s_cmd_tready_pin;
   wire 		axi_datamover_0_s_axis_mm2s_cmd_tvalid_pin;
   wire [71:0] 		axi_datamover_0_s_axis_s2mm_cmd_tdata_pin;
   wire 		axi_datamover_0_s_axis_s2mm_cmd_tvalid_pin;
   wire [127:0] 	axi_datamover_0_s_axis_s2mm_tdata_pin;
   wire 		axi_datamover_0_s_axis_s2mm_tlast_pin;
   wire 		axi_datamover_0_s_axis_s2mm_tready_pin;
   wire 		axi_datamover_0_s_axis_s2mm_tvalid_pin;
   wire 		axi_datamover_0_mm2s_err_pin;
   wire 		axi_datamover_0_s2mm_err_pin;
   wire   rst_n;
   wire   ck;
   wire   ck_n;
   wire   cke;
   wire   cs_n;
   wire   ras_n;
   wire   cas_n;
   wire   we_n;
   wire   [0:0]   dm_tdqs;
   wire   [2:0]   ba;
   wire   [15:0] addr;
   wire   [7:0]   dq;
   wire   [0:0]  dqs;
   wire   [0:0]  dqs_n;
   wire  [0:0]  tdqs_n;
   wire   odt;

   
   design_with_mb_and_dm_wrapper design_1_i 
     (
      .M_AXIS_MM2S_STS_tdata(),
      .M_AXIS_MM2S_STS_tkeep(),
      .M_AXIS_MM2S_STS_tlast(),
      .M_AXIS_MM2S_STS_tready(1'b1),
      .M_AXIS_MM2S_STS_tvalid(),
      .M_AXIS_MM2S_tdata(axi_datamover_0_m_axis_mm2s_tdata_pin),
      .M_AXIS_MM2S_tkeep(),
      .M_AXIS_MM2S_tlast(axi_datamover_0_m_axis_mm2s_tlast_pin),
      .M_AXIS_MM2S_tready(axi_datamover_0_m_axis_mm2s_tready_pin),
      .M_AXIS_MM2S_tvalid(axi_datamover_0_m_axis_mm2s_tvalid_pin),
      .M_AXIS_S2MM_STS_tdata(),
      .M_AXIS_S2MM_STS_tkeep(),
      .M_AXIS_S2MM_STS_tlast(),
      .M_AXIS_S2MM_STS_tready(1'b1),
      .M_AXIS_S2MM_STS_tvalid(),
      .S_AXIS_MM2S_CMD_tdata(axi_datamover_0_s_axis_mm2s_cmd_tdata_pin),
      .S_AXIS_MM2S_CMD_tready(axi_datamover_0_s_axis_mm2s_cmd_tready_pin),
      .S_AXIS_MM2S_CMD_tvalid(axi_datamover_0_s_axis_mm2s_cmd_tvalid_pin),
      .S_AXIS_S2MM_CMD_tdata(axi_datamover_0_s_axis_s2mm_cmd_tdata_pin),
      .S_AXIS_S2MM_CMD_tready(axi_datamover_0_s_axis_s2mm_cmd_tready_pin),
      .S_AXIS_S2MM_CMD_tvalid(axi_datamover_0_s_axis_s2mm_cmd_tvalid_pin),
      .S_AXIS_S2MM_tdata(axi_datamover_0_s_axis_s2mm_tdata_pin),
      .S_AXIS_S2MM_tkeep(16'hffff),
      .S_AXIS_S2MM_tlast(axi_datamover_0_s_axis_s2mm_tlast_pin),
      .S_AXIS_S2MM_tready(axi_datamover_0_s_axis_s2mm_tready_pin),
      .S_AXIS_S2MM_tvalid(axi_datamover_0_s_axis_s2mm_tvalid_pin),
      .mm2s_err(axi_datamover_0_mm2s_err_pin),
      .s2mm_err(axi_datamover_0_s2mm_err_pin),
      .user_clk(CLK),
      .user_reset(~RESET),
      .DDR3_SDRAM_addr(addr),
      .DDR3_SDRAM_ba(ba),
      .DDR3_SDRAM_cas_n(cas_n),
      .DDR3_SDRAM_ck_n(ck_n),
      .DDR3_SDRAM_ck_p(ck),
      .DDR3_SDRAM_cke(cke),
      .DDR3_SDRAM_cs_n(cs_n),
      .DDR3_SDRAM_dm(dm_tdqs),
      .DDR3_SDRAM_dq(dq),
      .DDR3_SDRAM_dqs_n(dqs_n),
      .DDR3_SDRAM_dqs_p(dqs),
      .DDR3_SDRAM_odt(odt),
      .DDR3_SDRAM_ras_n(ras_n),
      .DDR3_SDRAM_reset_n (rst_n),
      .DDR3_SDRAM_we_n(we_n),
      .sys_rst(~RESET),
      .sys_clk_p(sys_clk_p),
      .sys_clk_n(sys_clk_n)
      );
      
   ddr3_model ddr3_model_i (
   .rst_n(rst_n),
   .ck(ck),
   .ck_n(ck_n),
   .cke(cke),
   .cs_n(cs_n),
   .ras_n(ras_n),
   .cas_n(cas_n),
   .we_n(we_n),
   .dm_tdqs(dm_tdqs),
   .ba(ba),
   .addr(addr),
   .dq(dq),
   .dqs(dqs),
   .dqs_n(dqs_n),
   .tdqs_n(tdqs_n),
   .odt(odt)
);
   
   
   mkTb_for_nvm_controller mkTbNvmDDRConnection_i
     (
      .CLK(CLK),
      .RST_N(~RESET),        
      .ifc_read_data_ddr_ready_(axi_datamover_0_m_axis_mm2s_tready_pin),        
      .ifc_read_data_ddr__valid__is_valid(axi_datamover_0_m_axis_mm2s_tvalid_pin),       
      .ifc_read_data_ddr__data_in__data(axi_datamover_0_m_axis_mm2s_tdata_pin),
      .ifc_read_data_ddr__last__is_last(axi_datamover_0_m_axis_mm2s_tlast_pin),
      .ifc_read_cmd_ddr_valid_(axi_datamover_0_s_axis_mm2s_cmd_tvalid_pin),
      .ifc_read_cmd_ddr_data_(axi_datamover_0_s_axis_mm2s_cmd_tdata_pin),
      .ifc_read_cmd_ddr__ready__is_ready(axi_datamover_0_s_axis_mm2s_cmd_tready_pin),
      .ifc_write_data_ddr_valid_(axi_datamover_0_s_axis_s2mm_tvalid_pin),
      .ifc_write_data_ddr_data_out_(axi_datamover_0_s_axis_s2mm_tdata_pin),
      .ifc_write_data_ddr_last_(axi_datamover_0_s_axis_s2mm_tlast_pin),
					       .ifc_write_data_ddr__ready__is_ready(axi_datamover_0_s_axis_s2mm_tready_pin),
      .ifc_write_cmd_ddr_valid_(axi_datamover_0_s_axis_s2mm_cmd_tvalid_pin),
      .ifc_write_cmd_ddr_data_(axi_datamover_0_s_axis_s2mm_cmd_tdata_pin),
      .ifc_write_cmd_ddr__ready__is_ready(axi_datamover_0_s_axis_s2mm_cmd_tready_pin)
      );
   
endmodule
